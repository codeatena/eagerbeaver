package com.han.utility;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;

import com.github.snowdream.android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ShareFunc {
	
	@SuppressLint("DefaultLocale")
	public static boolean share(Context ctx, String nameApp, String content) {
	      try
	      {
	          List<Intent> targetedShareIntents = new ArrayList<Intent>();
	          Intent share = new Intent(android.content.Intent.ACTION_SEND);
	          share.setType("text/plain");
	          share.putExtra(android.content.Intent.EXTRA_TEXT, content);
	          share.putExtra(android.content.Intent.EXTRA_SUBJECT, content);
	          
	          List<ResolveInfo> resInfo = ctx.getPackageManager().queryIntentActivities(share, 0);
	          if (!resInfo.isEmpty()){
	              for (ResolveInfo info : resInfo) {
	                  Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
	                  
	                  targetedShare.setType("text/plain");
	                  targetedShare.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
	                  targetedShare.putExtra(android.content.Intent.EXTRA_SUBJECT, content);
	                  targetedShare.putExtra(android.content.Intent.EXTRA_TEXT, content);
	                  
	                  if (info.activityInfo.packageName.toLowerCase().contains(nameApp) || info.activityInfo.name.toLowerCase().contains(nameApp)) {    
	                      targetedShare.setPackage(info.activityInfo.packageName);
	                      targetedShare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
	                      ctx.startActivity(Intent.createChooser(targetedShare, "Share image using"));
	                      targetedShareIntents.add(targetedShare);
	                  }
	              }
	              Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Select app to share");
	              chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
	              
	              return true;
//	              startActivity(chooserIntent);
	          }
	      }
	      catch(Exception e){
	    	  
	          Log.e("social share exception: ", e);
	          return false;
	      }
	      
	      return false;
	}
	
    public static void showGeneralAlert(Context context, String title, String message) {
     	new AlertDialog.Builder(context)
     	
		 .setTitle(title)
		 .setMessage(message)
		 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int which) { 
		     // continue with delete
		     }
		  })
		  .show();
     }
}
