package com.eagerbeaver.eagerbeaver.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.eagerbeaver.eagerbeaver.MainActivity;
import com.eagerbeaver.eagerbeaver.R;
import com.han.social.Constants.Extra;
import com.han.social.FacebookActivity;
import com.han.social.FacebookEventObserver;
import com.han.utility.ShareFunc;

public class FragmentSettings extends FragmentBase implements OnClickListener {

	final String SHARE_CONTENT = "Check out Eager Beaver on Android. EB helps you track your relationships and improve communication. http://goo.gl/O0tf8q";
	
	public static String PAGEID = "SETTINGS";

	private FacebookEventObserver facebookEventObserver;
	
	ImageView btn_social_facebook;
	ImageView btn_social_sms;
	ImageView btn_social_email;
	ImageView btn_social_twitter;

	ImageView btn_twitter;
	ImageView btn_email;

	LinearLayout btn_settings_review;

	ImageView review_star1;
	ImageView review_star2;
	ImageView review_star3;
	ImageView review_star4;
	ImageView review_star5;

	ImageView review_stars[] = new ImageView[5];

	// constructor
	public FragmentSettings() {
		super();
		mStrPageID = PAGEID;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		FrameLayout view = (FrameLayout) inflater.inflate(
				R.layout.fragment_settings, container, false);

		/* Sharing Buttons */

		btn_social_facebook = (ImageView) view
				.findViewById(R.id.btn_social_facebook);
		btn_social_sms = (ImageView) view.findViewById(R.id.btn_social_sms);
		btn_social_email = (ImageView) view.findViewById(R.id.btn_social_email);
		btn_social_twitter = (ImageView) view
				.findViewById(R.id.btn_social_twitter);

		btn_social_facebook.setOnClickListener(this);
		btn_social_sms.setOnClickListener(this);
		btn_social_email.setOnClickListener(this);
		btn_social_twitter.setOnClickListener(this);

		/* Review Buttons */

		btn_settings_review = (LinearLayout) view
				.findViewById(R.id.btn_settings_review);
		btn_settings_review.setOnClickListener(this);

		review_stars[0] = (ImageView) view.findViewById(R.id.review_star1);
		review_stars[1] = (ImageView) view.findViewById(R.id.review_star2);
		review_stars[2] = (ImageView) view.findViewById(R.id.review_star3);
		review_stars[3] = (ImageView) view.findViewById(R.id.review_star4);
		review_stars[4] = (ImageView) view.findViewById(R.id.review_star5);

		review_stars[0].setOnClickListener(this);
		review_stars[1].setOnClickListener(this);
		review_stars[2].setOnClickListener(this);
		review_stars[3].setOnClickListener(this);
		review_stars[4].setOnClickListener(this);

		/* Contact Buttons */

		btn_twitter = (ImageView) view.findViewById(R.id.btn_twitter);
		btn_email = (ImageView) view.findViewById(R.id.btn_email);

		btn_twitter.setOnClickListener(this);
		btn_email.setOnClickListener(this);

		mRootLayout = view;
		
		facebookEventObserver = FacebookEventObserver.newInstance();
		
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_social_facebook:
//			ShareFunc.share(getActivity(), "facebook", SHARE_CONTENT);
//			openBrowser(Uri.parse("https://www.facebook.com/eagerbeaverapp"));
			startFacebookActivity();
			break;
		case R.id.btn_social_sms:
			openSMSClient(SHARE_CONTENT);
			break;
		case R.id.btn_social_email:
			openEmailClient("", SHARE_CONTENT);
			break;
		case R.id.btn_social_twitter:
			ShareFunc.share(getActivity(), "twitter", SHARE_CONTENT + " @eagerbeaverapp");
//			openBrowser(Uri.parse("https://twitter.com/eagerbeaverapp"));
//			startTwitterActivity();
			break;
		case R.id.btn_twitter:
			openBrowser(Uri.parse("https://twitter.com/eagerbeaverapp"));
			break;
		case R.id.btn_email:
			openEmailClient("info@eagerbeaverapp.com", "");
			break;
		case R.id.btn_settings_review:
			openBrowser(Uri.parse("https://play.google.com/store/apps/details?id=com.eagerbeaver.eagerbeaver"));
			break;
		/*
		 * case R.id.review_star1: reviewBtnClicked(0); break; case
		 * R.id.review_star2: reviewBtnClicked(1); break; case
		 * R.id.review_star3: reviewBtnClicked(2); break; case
		 * R.id.review_star4: reviewBtnClicked(3); break; case
		 * R.id.review_star5: reviewBtnClicked(4); break;
		 */
		default:
			break;
		}
	}

	private void openBrowser(Uri uri) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);

		startActivity(browserIntent);
	}

	private void openEmailClient(String email, String txt) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("plain/text");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
		intent.putExtra(Intent.EXTRA_SUBJECT, "");
		intent.putExtra(Intent.EXTRA_TEXT, txt);

		startActivity(Intent.createChooser(intent, ""));
	}

	private void openSMSClient(String content) {
		Intent sendIntent = new Intent(Intent.ACTION_VIEW);
		sendIntent.setData(Uri.parse("sms:"));
		sendIntent.putExtra("sms_body", content);

		startActivity(sendIntent);
	}

	/*
	 * private void reviewBtnClicked(int id) { for(int i = 0; i < 5; i++) { if
	 * (i <= id) {
	 * review_stars[i].setImageResource(R.drawable.settings_review_star_on); }
	 * else { review_stars[i].setImageResource(R.drawable.settings_review_star);
	 * } } }
	 */
	
	@Override
	public void onStart() {
		super.onStart();
		facebookEventObserver.registerListeners(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();
		facebookEventObserver.unregisterListeners();
	}

	private void startFacebookActivity() {
		Intent intent = new Intent(getActivity(), FacebookActivity.class);
		intent.putExtra(Extra.POST_MESSAGE, SHARE_CONTENT + " Fan page: fb.me/eagerbeaverapp");
//		intent.putExtra(Extra.POST_LINK, Constants.FACEBOOK_SHARE_LINK);
//		intent.putExtra(Extra.POST_LINK_NAME, Constants.FACEBOOK_SHARE_LINK_NAME);
//		intent.putExtra(Extra.POST_LINK_DESCRIPTION, Constants.FACEBOOK_SHARE_LINK_DESCRIPTION);
//		intent.putExtra(Extra.POST_PICTURE, Constants.FACEBOOK_SHARE_PICTURE);
		startActivity(intent);
	}
}
