package com.eagerbeaver.eagerbeaver.widgets;

import com.eagerbeaver.eagerbeaver.R;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.View.MeasureSpec;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

public class AvatarImageView extends ImageView{
	
	LayoutInflater mInflater;
	static PopupWindow bubble_popup = null;
	static AvatarImageView self = null;
	static View bubble_view = null;
	static int bubble_width = 0;
	
	public AvatarImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		self = this;

		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setAvatarId(int id) {
		
		int image_id = 0;
		int avatar_lbl_type = R.drawable.lbl_hooked;
		if (id == 0) {
			image_id = R.drawable.p_avatar_bored;
			avatar_lbl_type = R.drawable.lbl_bored;
		} else if (id == 1) {
			image_id = R.drawable.p_avatar_player;
			avatar_lbl_type = R.drawable.lbl_player;
		} else if (id == 2) {
			image_id = R.drawable.p_avatar_hooked;
			avatar_lbl_type = R.drawable.lbl_hooked;
		} else {
			image_id = R.drawable.p_avatar_thirsty;
			avatar_lbl_type = R.drawable.lbl_thirsty;
		}
		this.setImageDrawable(getResources().getDrawable(image_id));
		
		final int avatar_num = avatar_lbl_type;
		this.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showAvatarBubble(avatar_num);
			}
		});
	}
	
	private void showAvatarBubble(int avatar_lbl_type) {
		
		bubble_view = mInflater.inflate(R.layout.custom_marker_point, null);
		
		bubble_popup = new PopupWindow(this.getContext());

		ImageView avatar_img = (ImageView) bubble_view.findViewById(R.id.img_avatar_bubble);
		avatar_img.setImageDrawable(getResources().getDrawable(avatar_lbl_type));

		bubble_popup.setContentView(bubble_view);

		// Set content width and height
		bubble_popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		bubble_popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);

		// Closes the popup window when touch outside of it - when looses focus
		bubble_popup.setOutsideTouchable(true);
		bubble_popup.setFocusable(true);

		// Show anchored to button
		bubble_popup.setBackgroundDrawable(new ColorDrawable(this.getContext().getResources().getColor(android.R.color.transparent)));
		
		final int[] location = new int[2];
		this.getLocationInWindow(location);
		
		bubble_view.setLayoutParams(new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
		bubble_view.measure(MeasureSpec.makeMeasureSpec(0,  MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0,  MeasureSpec.UNSPECIFIED));
		bubble_width = bubble_view.getMeasuredWidth();
		
		bubble_popup.showAtLocation(this, Gravity.NO_GRAVITY, location[0] + (this.getMeasuredWidth() - bubble_width) / 2, location[1] + this.getMeasuredHeight());	
	}
}
