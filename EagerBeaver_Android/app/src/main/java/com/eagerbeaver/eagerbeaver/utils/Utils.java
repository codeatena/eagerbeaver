package com.eagerbeaver.eagerbeaver.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.Display;

import com.github.snowdream.android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Utils {

	@SuppressLint("NewApi")
	public static String getStringRepContentFromCursor(Cursor c) {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < c.getColumnCount(); i++) {
			builder.append(c.getColumnName(i)).append(":  ");
			switch(c.getType(i)) {
			case Cursor.FIELD_TYPE_STRING:
				builder.append(c.getString(i));
				break;
			case Cursor.FIELD_TYPE_INTEGER:
				builder.append(c.getInt(i));
				break;
			case Cursor.FIELD_TYPE_BLOB:
				builder.append("Blob(").append(c.getBlob(i).length);
				break;
			case Cursor.FIELD_TYPE_FLOAT:
				builder.append(c.getFloat(i));
				break;
			case Cursor.FIELD_TYPE_NULL:
				builder.append("NULL");
				break;
			}
			
			builder.append("\n");
		}
		
		return builder.toString();
	}
	
	
	public static Date getDateFromSMSDate(String date) {
		long dateLong = Long.parseLong(date);
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(dateLong);
		return cal.getTime();
	}
	
	
	public static Date getDateFromSMSDateSec(String date) {
		long dateLong = Long.parseLong(date);
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(dateLong * 1000);
		return cal.getTime();
	}
	
	/**
	 * get the screen size
	 */
	@SuppressWarnings("deprecation")
	public static int getScreenWidth(Activity activity) {
		Display display = activity.getWindowManager().getDefaultDisplay();
		return display.getWidth();
	}
	
	/**
	 * set the SP unit for layouts
	 */
	public static float setSpUnit(Context context, double size) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float orig = metrics.scaledDensity;
		metrics.scaledDensity = (float)size;
		return orig;
	}
	
	
	public static ArrayList<String> getAddressNumbers(Context context, long id) {
		ArrayList<String> arrNumbers = new ArrayList<String>();

		try {
			
		    String selectionAdd = new String("msg_id=" + id);
		    String uriStr = "content://mms/" + Long.toString(id) + "/addr";
		    Uri uriAddress = Uri.parse(uriStr);
		    Cursor cAdd = context.getContentResolver().query(uriAddress, null,
		        selectionAdd, null, null);
		    String name = null;
		    if (cAdd.moveToFirst()) {
		        do {
		            String number = cAdd.getString(cAdd.getColumnIndex("address"));
		            if (number != null) {
		                try {
		                    Long.parseLong(number.replace(" ", "").replace("+", "").replace("-", "").replace("(", "").replace(")", ""));
		                    name = number.replace(" ", "").replace("+", "").replace("-", "").replace("(", "").replace(")", "");
		                    arrNumbers.add(name);
		                } catch (NumberFormatException nfe) {
		                	
		                }
		            }
		        } while (cAdd.moveToNext());
		    }
		    if (cAdd != null) {
		        cAdd.close();
		    }

		    return arrNumbers;
		} 
		catch(Exception e) {
			Log.e("mms getAddressNumbers Exception: ", e);
            return null;
		}
 	}

    public static String getNumbersString(ArrayList<String> arrNumbers) {
        String strNumbers = " Numbers: ";
        for (String str: arrNumbers) {
            strNumbers += " " + str;
        }

        return strNumbers;
    }
	
	public static String getToastMsg(String region_id, String name) {
		String str = "";
		
		// Bored Region Message
		if( region_id.equals("010")) str= "Although " + name + " doesn't engage you that much, they don't seem to be bothered by it either.";
		else if( region_id.equals("011")) str= name + " is less responsive than most but maybe that's the way they communicate.";
		else if( region_id.equals("012")) str= name + " is only slightly interested in talking to you. Either step up communication or stop initiating.";
		else if( region_id.equals("020")) str= "Seems like " + name + " just isn't that into you. Maybe chill for a while";
		else if( region_id.equals("021")) str= "You're coming off as a little eager my friend. We recommend being a little chill.";
		else if( region_id.equals("022")) str= "You should try and get " + name + " to engage you more or give them a little break.";
		else if( region_id.equals("030")) str= "Dude, don't be creepy. Move on.";
		else if( region_id.equals("031")) str= "Let it go. Let it go! Seriously, give " + name + " a break.";
		else if( region_id.equals("032")) str= "You're proving that the thirst is real. Step away from the phone.";
		// Player region message
		else if( region_id.equals("110")) str= "Not too bad. " + name + " could reach out to you more but for the most part is showing some interest.";
		else if( region_id.equals("111")) str= name + " could be more into you but they still engage you.";
		else if( region_id.equals("120")) str= "You're putting in more energy than " + name + " so figure out if it's worth it.";
		else if( region_id.equals("121")) str= "Too cool " + name + " reaches out to you but just isn't that consistent.";
		else if( region_id.equals("130")) str= name + " is interested but not overly, just enough. Step it up and see if " + name + " does too.";
		else if( region_id.equals("131")) str= name + " communicates with you but could be more consistent.";
		// Hooked Region Message
		else if( region_id.equals("210")) str= "Keep doing what you're doing. You got " + name + " interested.";
		else if( region_id.equals("211")) str= name + " is putting in a little more effort than you are. Nothing wrong with that ;)";
		else if( region_id.equals("212")) str= "Isn't it great when someone puts in more effort to talk to you? That's what " + name + " is doing.";
		else if( region_id.equals("220")) str= name + " is all about you! Keep the texting going.";
		else if( region_id.equals("221")) str= "You're engaging " + name + " but they're engaging you a little more. Keep up the good communication.";
		else if( region_id.equals("222")) str= name + " texts you a lot more than you text them. Pretty cool.";
		else if( region_id.equals("230")) str= name + " just loves textying you. Really.";
		else if( region_id.equals("231")) str= "You're probably the first thing on " + name + "'s mind in the morning. Keep doing whatever you're doing.";
		else if( region_id.equals("232")) str= name + " enjoyes leading the conversation which is nice!";
		// Thirsty Region Message
		else if( region_id.equals("310")) str= name + " is pretty thirsty for you.";
		else if( region_id.equals("311")) str= "What did you say to make " + name + " go so cray for you?";
		else if( region_id.equals("312")) str= "Hand " + name + " a bottle of water. The thirst is real in this one.";
		else if( region_id.equals("320")) str= name + " thinks you're awesome. You're not even a little interested.";
		else if( region_id.equals("321")) str= name + " lived for the day they get a text from you.";
		else if( region_id.equals("322")) str= "Are you a model or something? " + name + " is thirsty for you.";
		else if( region_id.equals("330")) str= "Bye Felicia! Clearly you have no interest in talking to " + name;
		else if( region_id.equals("331")) str= "Tell " + name + " to download EagerBeaver. They need to know how thirsty they are.";
		else if( region_id.equals("332")) str= "Run!!! " + name + " might be a little too interested in you!";
		
		return str;
	}
	
	static int msg_count_bored = 3;
	static int msg_count_player = 2;
	static int msg_count_hooked = 3;
	static int msg_count_thirsty = 3;
	
	public static String getSuggestionMessage(String name, String region_id) {
		char region_num = region_id.charAt(0);
		if (region_num == '0') {			// Bored Region
			region_id = region_id + (int)(Math.random() * 1000) % msg_count_bored;
		} else if (region_num == '1') {		// Player Region
			region_id = region_id + (int)(Math.random() * 1000) % msg_count_player;
		} else if (region_num == '2') {		// Hooked Region
			region_id = region_id + (int)(Math.random() * 1000) % msg_count_hooked;
		} else {							// Thirsty Region
			region_id = region_id + (int)(Math.random() * 1000) % msg_count_thirsty;
		}
		return getToastMsg(region_id, name);
	}
}
