package com.eagerbeaver.eagerbeaver;

import android.app.Application;

import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo;

public class BaseApplication extends Application {
	private static BaseApplication g_app_instance;
	
	public static BaseApplication getInstance() {
		return g_app_instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		g_app_instance = this;
	}
	
	////////////////////////////////////////////////////////////////////
	// some global variables
	TotalStatsInfo totalStatsInfo = null;
	public TotalStatsInfo getTotalStatsInfo() {
		if (totalStatsInfo == null) {
			totalStatsInfo = new TotalStatsInfo();
		}
		return totalStatsInfo;
	}

	public void setTotalStatsInfo(TotalStatsInfo totalStatsInfo) {
		this.totalStatsInfo = totalStatsInfo;
	}
}
