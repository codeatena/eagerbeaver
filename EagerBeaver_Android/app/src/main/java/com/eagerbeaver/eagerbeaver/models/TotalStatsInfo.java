package com.eagerbeaver.eagerbeaver.models;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.eagerbeaver.eagerbeaver.models.DayStatsInfo.MsgMode;
import com.eagerbeaver.eagerbeaver.utils.SMSContentProvider;
import com.github.snowdream.android.util.Log;
import com.han.utility.PhoneUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TotalStatsInfo {
	public static final int NUM_DAYS_FOR_HISTORY = 7 * 2;
	
	public int m_nMsgs;
	public int m_nMsgsSent;
	
	public int m_nPictureMsgs;
	public int m_nPictureMsgsSent;
	
	public int m_totalLengthSent;
	public int m_totalLengthReceived;
	
	public ArrayList<SMSContact> m_ContactsInfo; // contacts
	public ArrayList<SMSContact> arrAllContacts;
	public ArrayList<DayStatsInfo> lastDaysStats;
	
	public ArrayList<PhoneModel> arrPhones;
	public ArrayList<DialogModel> arrDialogs;

	public long m_totalResponsesGet;
	public long m_totalResponsesGetTime; //in seconds
	
	public long m_totalResponds;
	public long m_totalRespondsTime; //in seconds
	
	
	public TotalStatsInfo() {
		
		lastDaysStats = new ArrayList<DayStatsInfo>();
		for(int i = 0; i < NUM_DAYS_FOR_HISTORY + 1; i++) {
			lastDaysStats.add(new DayStatsInfo());
		}
		
		m_nMsgs = 0;
		m_nMsgsSent = 0;
		
		m_nPictureMsgs = 0;
		m_nPictureMsgsSent = 0;
		
		m_totalLengthSent = 0;
		m_totalLengthReceived = 0;
		
		
		m_totalResponsesGet = 0;
		m_totalResponsesGetTime = 0;
		
		m_totalResponds = 0;
		m_totalRespondsTime = 0;
	}
	
	public class LastMsgInfo {
		public Date date;
		public boolean incoming;
		public int thread_id;
	}
	
	public enum MsgDirectionType {
		MsgDirectionNone,
		MsgDirectionIn,
		MsgDirectionOut
	}
	
	public String getContactFirstName(Context context, String id) {
		String whereName = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " = ?";
		String[] whereNameParams = new String[] { ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, id };
		Cursor nameCur = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, whereName, whereNameParams, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
		
		String given = null;
		//String family = null;
		//String display = null;
		
		if(nameCur.moveToFirst()) {
		    given = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
		    //family = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
		    //display = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
		}
		nameCur.close();
		
		return given;
	}
	
	public void readPhones(Context context) {
		arrPhones = new ArrayList<PhoneModel>();
		Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
		
		while (cursor.moveToNext()) {
			PhoneModel phoneModel = new PhoneModel();
			phoneModel.contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
			phoneModel.phoneNumber = PhoneUtility.refineNumber(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
			phoneModel.name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			
			Log.i("phoneModel: " + phoneModel.contactID + " " + phoneModel.name + " " + phoneModel.phoneNumber);
			if (phoneModel.phoneNumber == null)
				continue;
			if (phoneModel.phoneNumber.equals(""))
				continue;
			
			boolean isFind = false;
			
			for (SMSContact contact: arrAllContacts) {
				if (phoneModel.contactID.equals(contact.id)) {
					contact.addNumber(phoneModel.phoneNumber);
					isFind = true;
				}
			}
			
			if (!isFind) {
				SMSContact contact = new SMSContact(phoneModel.contactID,  phoneModel.name, null);
				contact.addNumber(phoneModel.phoneNumber);
				arrAllContacts.add(contact);
			}
			
			arrPhones.add(phoneModel);
		}
		
		m_ContactsInfo = new ArrayList<SMSContact>();

		for (SMSContact contact: arrAllContacts) {
			if (contact.addresses.size() > 0) {
				m_ContactsInfo.add(contact);
				String phones = "";
				for (String phoneNumber: contact.addresses) {
					phones += (" " + phoneNumber);
				}
				Log.i("contact data: " + contact.id + " " + contact.name + " " + phones);
			}
		}
		
		Log.i("phone count: " + Integer.toString(arrPhones.size()));
		Log.i("contact count: " + Integer.toString(m_ContactsInfo.size()));

		cursor.close();
	}
	
	public void readContacts(Context context) {
		Cursor cursor = SMSContentProvider.getAllContacts(context);
		arrAllContacts = new ArrayList<SMSContact>();
		
		cursor.moveToFirst();
		while (cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        	Log.i("sms contact: " + id + " " + name);
            String fname = null;//getContactFirstName(context, id);
        	SMSContact contact = new SMSContact(id, name, fname);
        	
        	arrAllContacts.add(contact);
		}
		cursor.close();
		Log.i("all contact count: " + Integer.toString(arrAllContacts.size()));
	}
	
	public void postProcessContacts(Context context) {
		for(int i = this.m_ContactsInfo.size() - 1; i >= 0 ; i--) {
			SMSContact contact = this.m_ContactsInfo.get(i);
			if(contact.cntMsgsBetween == 0)
			{
				this.m_ContactsInfo.remove(i);
			}
		}
		
		Log.i("last contact count: " + Integer.toString(m_ContactsInfo.size()));
	}
	
//	private void addContact(String id, String name, String f_name, String number) {
//		number = number.replace(" ", "").replace("-", "").replace("+", "");
//		
//		Log.v("Contacts", "id: " + id + " name: "+ name + " number: " + number);
//		
//		
//		boolean found  = false;
//		for(int i = 0; i < m_ContactsInfo.size(); i++) {
//			SMSContact cont = m_ContactsInfo.get(i);
//			if(cont.id.equals(id)) {
//				cont.addNumber(number);
//				found = true;
//			}
//		}
//		if(found == false) {
//			SMSContact contact = new SMSContact(id, number, name, f_name);
//			m_ContactsInfo.add(contact);
//		}
//	}
	
	public LastMsgInfo processMessageInConversation(MessageModel msg, LastMsgInfo lastInfo) {
//		int idxAddr = cursor.getColumnIndex("address");
//		String address = cursor.getString(idxAddr);
//		
//		String date = cursor.getString(cursor.getColumnIndex("date"));
//		Date smsDate = Utils.getDateFromSMSDate(date);
//		
//		String protocol = cursor.getString(cursor.getColumnIndex("protocol"));
//		boolean coming = (protocol != null);
//		
//		String body = cursor.getString(cursor.getColumnIndex("body"));
		
		String address = msg.address;
		
		if(address == null)
			return lastInfo;
		
		Log.i("processMessageInConversation: " + msg.address + " " + msg.incoming + " " + msg.sms);

		Date smsDate = msg.date;
		boolean coming = msg.incoming;
		String body = msg.body;
		int thread_id = msg.thread_id;
		
		//TODO: need to check if it is mms or sms
		boolean mms = !msg.sms;

		if(!mms)
			m_nMsgs++;
		else
			m_nPictureMsgs++;
		
		if(coming) {
			m_totalLengthReceived += body.length();
		}
		else {
			if(!mms)
				m_nMsgsSent++;
			else
				m_nPictureMsgsSent++;
			m_totalLengthSent += body.length();
		}
		
		MsgDirectionType msgType = MsgDirectionType.MsgDirectionNone; //0: none, 1: respond, 2: sending
		long diff = 0;
		if(lastInfo != null) {
			diff = (smsDate.getTime() - lastInfo.date.getTime()) / 1000; //in seconds
			if(diff < 24 * 60 * 60) {
				//should be less than 1 day
				if(lastInfo.incoming && !coming) {
					msgType = MsgDirectionType.MsgDirectionOut;
				}
				else if(!lastInfo.incoming && coming) {
					msgType = MsgDirectionType.MsgDirectionIn;
				}
			}
		}
		
		if(diff < 24 * 60 * 60) {
			if(msgType == MsgDirectionType.MsgDirectionOut) {
				m_totalResponds++;
				m_totalRespondsTime += diff;
			}
			else if(msgType == MsgDirectionType.MsgDirectionIn) {
				m_totalResponsesGet++;
				m_totalResponsesGetTime += diff;
			}
		}
		
		//per contact
		SMSContact contact = findContactWithNumber(address);
			
		if (contact != null) {
			contact.processMessage(coming, mms, smsDate, body.length(), msgType, diff, thread_id);
		}	

//		if(contact == null) {
//			//wow, found something, we need to add this
//			contact = new SMSContact(address, address, address, null);
//			m_ContactsInfo.add(contact);
//		}

		
		//daily stats
		Date nowDate = new Date();
		int daysFromToday = (int) ((nowDate.getTime() - smsDate.getTime()) / 1000 / 60 / 60 / 24);
		if(daysFromToday < NUM_DAYS_FOR_HISTORY) {
			// last x days trends
			lastDaysStats.get(daysFromToday).processMessage(coming, mms, body.length(), msgType, diff);
		} else {
			lastDaysStats.get(NUM_DAYS_FOR_HISTORY).processMessage(coming, mms, body.length(), msgType, diff);
		}
		
		LastMsgInfo newLastInfo = new LastMsgInfo();
		newLastInfo.date = smsDate;
		newLastInfo.incoming = coming;
		return newLastInfo;
	}
	
	private SMSContact findContactWithNumber(String number) {
		for(int i = 0 ; i < m_ContactsInfo.size(); i++) {
			SMSContact contact = m_ContactsInfo.get(i);
			if (contact.hasNumber(number))
				return contact;
		}
		return null;
	}
	
	public int getAveResponseTime()  {
		if(m_totalResponsesGet == 0) {
			return 0;
		}
		return (int)(m_totalResponsesGetTime / m_totalResponsesGet);
	}
	
	public int getAveRespondTime()  {
		if(m_totalResponds == 0) {
			return 0;
		}
		return (int)(m_totalRespondsTime / m_totalResponds);
	}
	
	public int getAveTime() {
		if(m_totalResponsesGet + m_totalResponds == 0)
			return 0;
		return (int)((m_totalResponsesGetTime + m_totalRespondsTime) / (m_totalResponsesGet + m_totalResponds)); 
	}
	
	public float getAveResTimesTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = (mode == MsgMode.MsgModeSend) ? info.m_totalResponds : info.m_totalResponsesGet;
		long msgRespTime = (mode == MsgMode.MsgModeSend) ? info.m_totalRespondsTime : info.m_totalResponsesGetTime;
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.m_totalResponds : info.m_totalResponsesGet;
			msgRespTime += (mode == MsgMode.MsgModeSend) ? info.m_totalRespondsTime : info.m_totalResponsesGetTime;
			
			values[i] = (msgCount == 0) ? 0 : msgRespTime / msgCount;
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////
	public float getAveResTimesLast7Days(MsgMode mode) {
		
		int weekDaysCount = Math.min(7, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long totalTime = 0;
		
		for(int i = 0; i < weekDaysCount; i++) {
			DayStatsInfo info = lastDaysStats.get(i);
			if(mode == MsgMode.MsgModeSend) {
				totalTime += info.getAveRespondTime();
			}
			else if(mode == MsgMode.MsgModeRecv) {
				totalTime += info.getAveResponseTime();
			}
			else {
				totalTime += info.getAveTime();
			}
		}
		
		return totalTime / weekDaysCount;
	}
	
	public float getAveResTimesLast7DaysTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			DayStatsInfo info = lastDaysStats.get(i);
			if(mode == MsgMode.MsgModeSend) {
				values[i] = info.getAveRespondTime();
			}
			else if(mode == MsgMode.MsgModeRecv) {
				values[i] = info.getAveResponseTime();
			}
			else {
				values[i] = info.getAveTime();
			}
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	
	public long getMessagesCountLast7Days(MsgMode mode) {
		int dayCount = Math.min(7, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = 0;
		for(int i = 0; i < dayCount; i++) {
			DayStatsInfo info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
		}
		return msgCount;
	}
	
	public float getMessageCountTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
			values[i] = msgCount;
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	
	public float getMessageCountLast7DaysTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			msgCount = (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
			values[i] = msgCount;
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	
	public void readMessages(Context context) {
		
		arrDialogs = new ArrayList<DialogModel>();
		
		ContentResolver cr = context.getContentResolver();
		
		long lastTime = Calendar.getInstance().getTimeInMillis() - (long) ((long) 30 * (long) 86400000);
				
		Cursor smsCur = cr.query(Uri.parse("content://sms"), null,  "date>" + Long.toString(lastTime) , null, null);
		
		try {
			if (smsCur.moveToFirst()) {
				do {
					MessageModel newMsg = MessageModel.create(context, smsCur, true);
					if (newMsg != null)
						addMessage(newMsg);
				} while (smsCur.moveToNext());
			}
		} finally {
			smsCur.close();
		}
		
		Cursor mmsCur = cr.query(Uri.parse("content://mms"), null,  "date>" + Long.toString(lastTime / (long) 1000), null, null);
		
		try {
			if (mmsCur.moveToFirst()) {
				do {
					MessageModel newMsg = MessageModel.create(context, mmsCur, false);
					if (newMsg != null)
						addMessage(newMsg);
				} while (mmsCur.moveToNext());
			}
		} finally {
			mmsCur.close();
		}
		
		for(DialogModel dialog: arrDialogs) {
			dialog.sortbyDate();
		}
		
		for(DialogModel dialog: arrDialogs) {
			LastMsgInfo lastInfo = null;
			for (MessageModel msg: dialog.msgs) {
				Log.i("dialog: " + dialog.thread_id, "msg " + msg._id + ":" + msg.date);
				lastInfo = processMessageInConversation(msg, lastInfo);
			}
		}
	}
	
	private void addMessage(MessageModel message) {
		
		DialogModel curDialog = null;
		
		for (DialogModel dialog: arrDialogs) {
			if (dialog.thread_id == message.thread_id) {
				curDialog = dialog;
				break;
			}
		}
		if (curDialog == null) {
			curDialog = new DialogModel(message.thread_id);
			arrDialogs.add(curDialog);
		}
		
		curDialog.msgs.add(message);
	}
	
	public String getRegionNumber() {

		float cx = 100.0f;
		float cy = 100.0f;
		
		float x = -cx + 2.0f * cx * getRatioX() / 100.0f;
		float y = cy - 2.0f * cy * getRatioY() / 100.0f;
		
		if (x <= 0.0f && y >= 0.0f){
			if (x >= -(cx / 3.0f) && y <= cy / 3.0f) {
				return ("01");
			} else if (x >= -(2.0f * cx / 3.0f) && y <= (2.0f * cy / 3.0f)) {
				return ("02");
			} else {
				return ("03");
			}
		} else if (x <= 0.0f && y <= 0.0f) {
			if (x >= -(cx / 3.0f) && y >= -(cy / 3.0f)) {
				return ("11");
			} else if (x >= -(2.0f * cx / 3.0f) && y >= -(2.0f * cy / 3.0f)) {
				return ("12");
			} else {
				return ("13");
			}
		} else if (x >= 0.0f && y >= 0.0f) {
			if (x <= cx / 3.0f && y <= cy / 3.0f) {
				return ("21");
			} else if (x <= (2.0f * cx / 3.0f) && y <= (2.0f * cy / 3.0f)) {
				return ("22");
			} else {
				return ("23");
			}
		} else {
			if (x <= cx / 3.0f && y >= -(cy / 3.0f)) {
				return ("31");
			} else if (x <= (2.0f * cx / 3.0f) && y >= -(2.0f * cy / 3.0f)) {
				return ("32");
			} else {
				return ("33");
			}
		}
	}
	
	public long getAveCharLength(MsgMode mode) {
		if(mode == MsgMode.MsgModeSend) {
			return m_nMsgsSent == 0 ? 0 : m_totalLengthSent / m_nMsgsSent;
		}
		else if(mode == MsgMode.MsgModeRecv) {
			return (m_nMsgs - m_nMsgsSent) == 0 ? 0 : (m_totalLengthReceived) / (m_nMsgs - m_nMsgsSent);
		}
		return m_nMsgs == 0 ? 0 : (m_totalLengthSent + m_totalLengthReceived) / m_nMsgs;
	}
	
	public int getRatioX() {
		float charLength = (float) this.getAveCharLength(MsgMode.MsgModeTotal);
		
		if(charLength <= 20.0f) {
			charLength = charLength * 25.0f / 20.0f;
		}
		else if(charLength <= 35.0f) {
			charLength = 25.0f + (charLength - 20.0f) * 25.0f / 10.0f;
		}
		else if(charLength <= 60.0f) {
			charLength = 50.0f + (charLength - 35.0f) * 25.0f / 25.0f;
		}
		else {
			charLength = 75.0f + (charLength - 60.0f) * 25.0f / 40.0f;
		}
		
		float resptime = (float) this.getAveResponseTime();
		if(resptime >= 90.0f) {
			resptime = 25.0f + (resptime - 90.0f) * -25 / 1350;
		}
		else if(resptime >= 30) {
			resptime = 50 + (resptime - 30) * -25 / 60;
		}
		else if(resptime >= 10) {
			resptime = 75 + (resptime - 10) * -25 / 20;
		}
		else {
			resptime = 100 + (resptime) * -25 / 10;
		}
		
		float finalX = charLength * 0.3f + resptime * 0.7f;
		
		if(finalX > 100)
			finalX = 100;
		else if(finalX < 0)
			finalX = 0;
		
		return (int)finalX;
	}
	
	public int getRatioY() {
		float M = m_nMsgsSent == 0 ? 5.0f : (float) (((float) this.m_nMsgs - (float) this.m_nMsgsSent) / (float) this.m_nMsgsSent);
		
		float newM = M;
		if(M <= 0.65f) {
			newM = M * 25 / 0.65f;
		}
		else if (M <= 0.8f){
			newM = 25.0f + (M - 0.65f) * (25.0f / 0.15f);
		}
		else if(M <= 1.20f) {
			newM = 50.0f + (M - 0.8f) * (25.0f / 0.40f);
		}
		else {
			newM = 75.0f + (M - 1.20f) * (25.0f / 3.8f);
		}
		
		float PM = this.m_nPictureMsgsSent == 0 ? 5 : (this.m_nPictureMsgs - this.m_nPictureMsgsSent) / this.m_nPictureMsgsSent;
		
		float newPM = PM;
		if(PM <= 0.65f) {
			newPM = PM * 25.0f / 0.65f;
		}
		else if(PM <= 0.8f){
			newPM = 25.0f + (PM - 0.65f) * (25.0f / 0.15f);
		}
		else if(PM <= 1.20f) {
			newPM = 50.0f + (PM - 0.8f) * (25.0f / 0.40f);
		}
		else {
			newPM = 75.0f + (PM - 1.20f) * (25.0f / 3.8f);
		}
		
		float finalY = newM * 0.85f + newPM * 0.15f;
		
		if(finalY > 100.0f)
			finalY = 100.0f;
		else if(finalY < 0)
			finalY = 0.0f;
		
		return (int)finalY;
	}
}
