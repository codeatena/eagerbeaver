package com.eagerbeaver.eagerbeaver.analyzers;

import android.content.Context;

import com.eagerbeaver.eagerbeaver.models.MessageModel;
import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo;

import java.util.ArrayList;
import java.util.Date;
public class MSGAnalyzer {
	
	public static TotalStatsInfo analyzeTopStats(Context context) {
		
		//first pre-store all of conversations
		
		TotalStatsInfo statsInfo = new TotalStatsInfo();
		
		statsInfo.readContacts(context);
		statsInfo.readPhones(context);
		
		statsInfo.readMessages(context);
		
		statsInfo.postProcessContacts(context);
		
		return statsInfo;
	}
	
	//this function basically do nothing more than inserting newMsg to a correct location 
	//so that the array will always be in a sorted order by "date"
	protected static void addMessageToCorrectLocation(ArrayList<MessageModel> msgs, MessageModel newMsg) {
		
		//check date
		Date now = new Date();
		long diff = now.getTime() - newMsg.date.getTime();
		if(diff / (24 * 60 * 60 * 1000) > 30) {
			//old than 30 days
			//don't add
			return;
		}
		
		boolean added = false;
		for(int i = 0; i < msgs.size(); i++)
		{
			MessageModel msg = msgs.get(i);
			if(msg.date.getTime() > newMsg.date.getTime()) {
				msgs.add(i, newMsg);
				added = true;
				break;
			}
		}
		if(added == false) {
			msgs.add(newMsg);
		}
	}
	
}
