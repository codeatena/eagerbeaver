package com.eagerbeaver.eagerbeaver.fragments;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eagerbeaver.eagerbeaver.BaseApplication;
import com.eagerbeaver.eagerbeaver.MainActivity;
import com.eagerbeaver.eagerbeaver.R;
import com.eagerbeaver.eagerbeaver.models.DayStatsInfo;
import com.eagerbeaver.eagerbeaver.models.DayStatsInfo.MsgMode;
import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo;
import com.eagerbeaver.eagerbeaver.widgets.MyMarkerView;
import com.eagerbeaver.eagerbeaver.widgets.MyScrollView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Legend;
import com.github.mikephil.charting.utils.Legend.LegendForm;
import com.github.mikephil.charting.utils.Legend.LegendPosition;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.XLabels.XLabelPosition;
import com.github.snowdream.android.util.Log;
import com.han.utility.TimeUtility;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;

import java.util.ArrayList;
import java.util.Date;

public class FragmentTopStats extends FragmentBase implements OnChartValueSelectedListener, OnClickListener {
	
	public static String PAGEID = "SMSContent"; 
	
	//constructor
	public FragmentTopStats() {
		super();
		mStrPageID = PAGEID;
	}
	
	public enum ViewMode {
		ViewModeResponseTime,
		ViewModeNumMessages
	}
	
	ViewMode mViewMode = ViewMode.ViewModeResponseTime;
	
	TotalStatsInfo mInfo;
	
	//UI elements
	RelativeLayout mBtnResponseTime;
	RelativeLayout mBtnNumMsgs;
	
	ImageView mIco_respBtn;
	ImageView mIco_msgCount;
	TextView mTxt_respBtn;
	TextView mTxt_msgCount;
	View mIndi_respBtn;
	View mIndi_msgCount;
	
	MyScrollView fragment_content_scroll;
	LinearLayout container_response_time;
	LinearLayout container_msg_count;
	
	RelativeLayout banner_interest;
	ImageView img_banner_interest;
	ImageView btn_exit_banner;
	
	FrameLayout mLayoutGraphContainer;
	GraphView mGraphView;
	
	//various TextViews;
//	TextView mTV_Header[] = new TextView[4];
	TextView mTV[][] = new TextView[4][4];
	
	RelativeLayout bar_chart_container;
	RelativeLayout line_chart_container;
	
	LineChart resp_chart;
	BarChart msg_chart;
	
	ImageView img_your_interest;
	TextView txt_your_interest;
	ImageView img_peeps_interest;
	TextView txt_peeps_interest;
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		LinearLayout view = (LinearLayout)inflater.inflate(R.layout.fragment_top_stats, container, false);
		
		mRootLayout = view;
		
		mIco_respBtn = (ImageView) view.findViewById(R.id.img_respButton);
		mIco_msgCount = (ImageView) view.findViewById(R.id.img_msgButton);
		mTxt_respBtn = (TextView) view.findViewById(R.id.txt_respButton);
		mTxt_msgCount = (TextView) view.findViewById(R.id.txt_msgButton);
		mIndi_respBtn = (View) view.findViewById(R.id.btn_respIndicator);
		mIndi_msgCount = (View) view.findViewById(R.id.btn_msgIndicator);
		
		mBtnResponseTime = (RelativeLayout)view.findViewById(R.id.btnResponseTime);
		mBtnNumMsgs = (RelativeLayout)view.findViewById(R.id.btnNumberMsgs);
		
		mBtnResponseTime.setOnClickListener(mClickListener);
		mBtnNumMsgs.setOnClickListener(mClickListener);
		
		fragment_content_scroll = (MyScrollView) view.findViewById(R.id.fragment_content_scroll);
		fragment_content_scroll.setEnableScrolling(true);
		container_response_time = (LinearLayout) view.findViewById(R.id.container_response_time);
		container_msg_count = (LinearLayout) view.findViewById(R.id.container_msg_count);
		fragment_content_scroll.setVisibility(View.VISIBLE);
//		container_response_time.setVisibility(View.VISIBLE);
		container_msg_count.setVisibility(View.GONE);
		
		// Banner Region
		banner_interest = (RelativeLayout) view.findViewById(R.id.banner_interest);
		img_banner_interest = (ImageView) view.findViewById(R.id.img_banner_interest);
		btn_exit_banner = (ImageView) view.findViewById(R.id.btn_exit_banner);
//		setBannerStyle(2);
		
		btn_exit_banner.setOnClickListener(this);
		
		// Content Region
		
		msg_chart = (BarChart) view.findViewById(R.id.msg_chart);
		resp_chart = (LineChart) view.findViewById(R.id.resp_chart);
		
		img_your_interest = (ImageView)view.findViewById(R.id.img_your_interest);
		txt_your_interest = (TextView)view.findViewById(R.id.txt_your_interest);
		img_peeps_interest = (ImageView)view.findViewById(R.id.img_peeps_interest);
		txt_peeps_interest = (TextView)view.findViewById(R.id.txt_peeps_interest);
		
		mTV[0][0] = (TextView)view.findViewById(R.id.txt01);
		mTV[0][1] = (TextView)view.findViewById(R.id.txt02);
		mTV[0][2] = (TextView)view.findViewById(R.id.txt01_ave);
		mTV[0][3] = (TextView)view.findViewById(R.id.txt02_ave);
		
		mTV[1][0] = (TextView)view.findViewById(R.id.txt11);
		mTV[1][1] = (TextView)view.findViewById(R.id.txt12);
		mTV[1][2] = (TextView)view.findViewById(R.id.txt11_ave);
		mTV[1][3] = (TextView)view.findViewById(R.id.txt12_ave);
		
		mTV[2][0] = (TextView)view.findViewById(R.id.txt21);
		mTV[2][1] = (TextView)view.findViewById(R.id.txt22);
		mTV[2][2] = (TextView)view.findViewById(R.id.txt21_ave);
		mTV[2][3] = (TextView)view.findViewById(R.id.txt22_ave);
		
		mTV[3][0] = (TextView)view.findViewById(R.id.txt31);
		mTV[3][1] = (TextView)view.findViewById(R.id.txt32);
		mTV[3][2] = (TextView)view.findViewById(R.id.txt31_ave);
		mTV[3][3] = (TextView)view.findViewById(R.id.txt32_ave);
		
		// Future Update
		mTV[0][2].setVisibility(View.GONE);
		mTV[0][3].setVisibility(View.GONE);
		mTV[1][2].setVisibility(View.GONE);
		mTV[1][3].setVisibility(View.GONE);
		mTV[2][2].setVisibility(View.GONE);
		mTV[2][3].setVisibility(View.GONE);
		mTV[3][2].setVisibility(View.GONE);
		mTV[3][3].setVisibility(View.GONE);
		
		showMetrics(ViewMode.ViewModeResponseTime);
		
		return view;
	}
		
	protected void showMetrics(ViewMode mode) {		
		mInfo = ((BaseApplication)getActivity().getApplication()).getTotalStatsInfo();
		
		if(mInfo == null)
			return;
		
		int avatar_id;
		if (mInfo.getRegionNumber().charAt(0) == '0') {
			avatar_id = 1;											//Bored
		} else if (mInfo.getRegionNumber().charAt(0) == '1') {		
			avatar_id = 0;											//Player
		} else if (mInfo.getRegionNumber().charAt(0) == '2') {
			avatar_id = 2;											//Hooked
		} else {	
			avatar_id = 3;											//Thirsty
		}
		
		setBannerStyle(avatar_id);
		
		float trends = 0;
		
		mViewMode = mode;
		if(mViewMode == ViewMode.ViewModeResponseTime) {
			
			mIco_respBtn.setImageResource(R.drawable.ico_resptime_on);
			mIco_msgCount.setImageResource(R.drawable.ico_msgcount_off);
			mTxt_respBtn.setTextColor(getResources().getColor(R.color.tab_bg));
			mTxt_msgCount.setTextColor(getResources().getColor(R.color.tab_div_col));
			mIndi_respBtn.setBackgroundColor(getResources().getColor(R.color.tab_bg));
			mIndi_msgCount.setBackgroundColor(Color.TRANSPARENT);
			
			fragment_content_scroll.setVisibility(View.VISIBLE);
			container_msg_count.setVisibility(View.GONE);
			
			drawLineChart();
			
			mTV[0][0].setText(String.format("%.2f", (float)(mInfo.getAveRespondTime() / 60)));
			trends = mInfo.getAveResTimesTrend(MsgMode.MsgModeSend);
			setAveBgText(trends, mTV[0][2]);
			
			mTV[0][1].setText(String.format("%.2f", (float)(mInfo.getAveResTimesLast7Days(MsgMode.MsgModeSend) / 60)));
			trends = mInfo.getAveResTimesLast7DaysTrend(MsgMode.MsgModeSend);
			setAveBgText(trends, mTV[0][3]);
			
//			mTV[0][3].setText("+" + String.format("%.2f", mInfo.getAveLast));

			mTV[1][0].setText(String.format("%.2f", (float)(mInfo.getAveResponseTime() / 60)));
			trends = mInfo.getAveResTimesTrend(MsgMode.MsgModeRecv);
			setAveBgText(trends, mTV[1][2]);
			
			mTV[1][1].setText(String.format("%.2f", (float)(mInfo.getAveResTimesLast7Days(MsgMode.MsgModeRecv) / 60)));
			trends = mInfo.getAveResTimesLast7DaysTrend(MsgMode.MsgModeRecv);
			setAveBgText(trends, mTV[1][3]);
			
		}
		else if(mViewMode == ViewMode.ViewModeNumMessages) {
			
			mIco_respBtn.setImageResource(R.drawable.ico_resptime_off);
			mIco_msgCount.setImageResource(R.drawable.ico_msgcount_on);
			mTxt_respBtn.setTextColor(getResources().getColor(R.color.tab_div_col));
			mTxt_msgCount.setTextColor(getResources().getColor(R.color.tab_bg));
			mIndi_respBtn.setBackgroundColor(Color.TRANSPARENT);
			mIndi_msgCount.setBackgroundColor(getResources().getColor(R.color.tab_bg));
			
			fragment_content_scroll.setVisibility(View.GONE);
			container_msg_count.setVisibility(View.VISIBLE);
			
			GraphViewData[] data = new GraphViewData[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
			for(int i = 0; i < TotalStatsInfo.NUM_DAYS_FOR_HISTORY; i++) {
				DayStatsInfo info = mInfo.lastDaysStats.get(i);
				data[i] = new GraphViewData(i, info.numberOfMessages);
			}
			drawBarChart();
			
			mTV[2][0].setText(Long.toString(mInfo.getMessagesCountLast7Days(MsgMode.MsgModeSend)));
			trends = mInfo.getMessageCountLast7DaysTrend(MsgMode.MsgModeSend); //no trend
			setAveBgText(trends, mTV[2][2]);
			
			mTV[2][1].setText(Long.toString(mInfo.getMessagesCountLast7Days(MsgMode.MsgModeRecv)));
			trends = mInfo.getMessageCountLast7DaysTrend(MsgMode.MsgModeRecv);
			setAveBgText(trends, mTV[2][3]);

			
			mTV[3][0].setText(Integer.toString(mInfo.m_nMsgsSent));
			trends = mInfo.getMessageCountTrend(MsgMode.MsgModeSend);
			setAveBgText(trends, mTV[3][2]);
			
			mTV[3][1].setText(Integer.toString(mInfo.m_nMsgs - mInfo.m_nMsgsSent));
			trends = mInfo.getMessageCountTrend(MsgMode.MsgModeRecv);
			setAveBgText(trends, mTV[3][3]);
		}
	}
	
	private void setAveBgText(Float v_trends, TextView t_view) {
		GradientDrawable drawable = (GradientDrawable) t_view.getBackground();
		if (v_trends > 0) {
			drawable.setColor(getResources().getColor(R.color.color_green));
			t_view.setText("+" + Float.toString(v_trends) + "%");	
		} else if (v_trends == 0){
			drawable.setColor(getResources().getColor(R.color.color_red));
			t_view.setText("+" + Float.toString(v_trends) + "%");
		} else {
			drawable.setColor(getResources().getColor(R.color.color_red));
			t_view.setText(Float.toString(v_trends) + "%");
		}
	}
	
	private void setBannerStyle(int style_id) {
		switch(style_id) {
			case 0:	// Player Banner
				img_banner_interest.setImageResource(R.drawable.banner_player);
				btn_exit_banner.setImageResource(R.drawable.banner_cross_brown);
				break;
			case 1: // Bored Banner
				img_banner_interest.setImageResource(R.drawable.banner_bored);
				btn_exit_banner.setImageResource(R.drawable.banner_cross_brown);
				break;
			case 2: // Hooked Banner
				img_banner_interest.setImageResource(R.drawable.banner_hooked);
				btn_exit_banner.setImageResource(R.drawable.banner_cross_white);
				break;
			case 3: // Thirsty Banner
				img_banner_interest.setImageResource(R.drawable.banner_thirsty);
				btn_exit_banner.setImageResource(R.drawable.banner_cross_white);
				break;
			default:
				break;
		}
	}
	
	private void drawLineChart() {
		resp_chart.setOnChartValueSelectedListener(this);
		resp_chart.setStartAtZero(true);
		
		resp_chart.setDrawYValues(false);
		resp_chart.setDrawYLabels(false);
		
		resp_chart.setGridColor(Color.TRANSPARENT);
		resp_chart.setDrawGridBackground(false);
		
		resp_chart.setDrawBorder(false);
		
		resp_chart.setNoDataTextDescription("You need to provide data for the chart.");
		resp_chart.setDescription("");
		
		resp_chart.setHighlightEnabled(true);
		resp_chart.setTouchEnabled(true);
		
		resp_chart.setPinchZoom(true);
		
		MyMarkerView mv_bubble = new MyMarkerView(mMainActivity, R.layout.custom_marker_view);
		resp_chart.setMarkerView(mv_bubble);
		
		resp_chart.setHighlightIndicatorEnabled(false);
		
		setLineData(7);
		
		Legend l = resp_chart.getLegend();
		l.setForm(LegendForm.CIRCLE);
		l.setPosition(LegendPosition.RIGHT_OF_CHART_INSIDE);
		l.setTextColor(getResources().getColor(R.color.color_text_main));
		l.setTextSize(10.0f);
		
		XLabels xl = resp_chart.getXLabels();
		
		xl.setSpaceBetweenLabels(2);
		xl.setPosition(XLabelPosition.BOTTOM);
	}
	
	private void setLineData(int count) {

		ArrayList<String> xVals = new ArrayList<String>();
		Date nowDate = new Date();
		for (int i = count; i > 0; i--) {
//			xVals.add("Day " + (i + 1));
			Date date = new Date();
			date.setTime(nowDate.getTime() - (long) 86400000 * (long) (i - 1));
			xVals.add(TimeUtility.getStringFromDate(date, "MM/dd"));
		}

        ArrayList<Entry> yVals_you = new ArrayList<Entry>();
        ArrayList<Entry> yVals_contact = new ArrayList<Entry>();

        int weekDaysCount = Math.min(count, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		for(int i = 0; i < weekDaysCount; i++) {
			DayStatsInfo info = mInfo.lastDaysStats.get(weekDaysCount - i - 1);
			yVals_you.add(new Entry(info.getAveRespondTime() / 60, i));
			yVals_contact.add(new Entry(info.getAveResponseTime() / 60, i));
		}
		
		
        // create a dataset and give it a type
        LineDataSet dataset_you = new LineDataSet(yVals_you, "You");

        dataset_you.setColor(getResources().getColor(R.color.chart_you));
        dataset_you.setCircleColor(Color.TRANSPARENT);
        dataset_you.setLineWidth(3f);
        dataset_you.setCircleSize(1f);
        
        // create a dataset and give it a type
        LineDataSet dataset_contact = new LineDataSet(yVals_contact, "Your peeps");

        dataset_contact.setColor(getResources().getColor(R.color.chart_contact));
        dataset_contact.setCircleColor(Color.TRANSPARENT);
        dataset_contact.setFillColor(Color.TRANSPARENT);
        dataset_contact.setLineWidth(3f);
        dataset_contact.setCircleSize(1f);

        
        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(dataset_you); // add the datasets
        dataSets.add(dataset_contact); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);

        // set data
        resp_chart.setData(data);
        resp_chart.animateX(3000);
        resp_chart.invalidate();
    }
	
	private void drawBarChart() {
		msg_chart.setOnChartValueSelectedListener(this);
		msg_chart.setStartAtZero(true);
		
		msg_chart.setDrawYValues(false);
		msg_chart.setDrawYLabels(false);
		
		msg_chart.setGridColor(Color.TRANSPARENT);
		msg_chart.setDrawGridBackground(false);
		msg_chart.setDrawBarShadow(false);
		
		msg_chart.setDrawBorder(false);
		
		msg_chart.setNoDataTextDescription("You need to provide data for the chart.");
		msg_chart.setDescription("");
		
		msg_chart.setHighlightEnabled(true);
		msg_chart.setTouchEnabled(true);
		
		msg_chart.setPinchZoom(true);
		
		MyMarkerView mv_bubble = new MyMarkerView(mMainActivity, R.layout.custom_marker_view);
		msg_chart.setMarkerView(mv_bubble);
		
		msg_chart.setHighlightIndicatorEnabled(false);
		
		setBarData(7);
		
		Legend l = msg_chart.getLegend();
		l.setForm(LegendForm.CIRCLE);
		l.setPosition(LegendPosition.RIGHT_OF_CHART_INSIDE);
		l.setTextColor(getResources().getColor(R.color.color_text_main));
		l.setTextSize(10.0f);
		
		XLabels xl = msg_chart.getXLabels();
		
		xl.setSpaceBetweenLabels(2);
		xl.setPosition(XLabelPosition.BOTTOM);
	}
	
	private void setBarData(int count) {

		ArrayList<String> xVals = new ArrayList<String>();
		Date nowDate = new Date();
		for (int i = count; i > 0; i--) {
//			xVals.add("Day " + (i + 1));
			Date date = new Date();
			date.setTime(nowDate.getTime() - (long) 86400000 * (long) (i - 1));
			xVals.add(TimeUtility.getStringFromDate(date, "MM/dd"));
		}
		
        ArrayList<BarEntry> yVals_you = new ArrayList<BarEntry>();
        ArrayList<BarEntry> yVals_contact = new ArrayList<BarEntry>();
        
        int weekDaysCount = Math.min(count, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
        for(int i = 0; i < weekDaysCount; i++) {
			DayStatsInfo info = mInfo.lastDaysStats.get(weekDaysCount - i - 1);
			yVals_you.add(new BarEntry(info.getNumberOfMessages(MsgMode.MsgModeSend) / 60, i));
			yVals_contact.add(new BarEntry(info.getNumberOfMessages(MsgMode.MsgModeRecv) / 60, i));
		}

        // create a dataset and give it a type
        BarDataSet dataset_you = new BarDataSet(yVals_you, "You");
        dataset_you.setColor(getResources().getColor(R.color.chart_you));
        
        BarDataSet dataset_contact = new BarDataSet(yVals_contact, "Your peeps");
        dataset_contact.setColor(getResources().getColor(R.color.chart_contact));
        
        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(dataset_you); // add the datasets
        dataSets.add(dataset_contact); // add the datasets

        // create a data object with the datasets
        BarData data = new BarData(xVals, dataSets);

        data.setGroupSpace(110f);
        
        // set data
        msg_chart.setData(data);
        msg_chart.animateY(3000);
        msg_chart.invalidate();
    }
	
	View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(v == mBtnNumMsgs) {
				showMetrics(ViewMode.ViewModeNumMessages);
			}
			else if(v == mBtnResponseTime) {
				showMetrics(ViewMode.ViewModeResponseTime);
			}
		}
	};

	@Override
	public void onValueSelected(Entry e, int dataSetIndex) {
		// TODO Auto-generated method stub
		Log.i("VAL Selected: " + "Value: " + e.getVal() + ", xIndex: " + e.getXIndex() + ", DateSet index: " + dataSetIndex);
	}

	@Override
	public void onNothingSelected() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
		case R.id.btn_exit_banner:
			banner_interest.setVisibility(View.GONE);
			break;
		default:
			break;
		}
	}
	
}
