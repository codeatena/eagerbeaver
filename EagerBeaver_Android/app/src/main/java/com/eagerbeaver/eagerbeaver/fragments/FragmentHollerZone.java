package com.eagerbeaver.eagerbeaver.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eagerbeaver.eagerbeaver.BaseApplication;
import com.eagerbeaver.eagerbeaver.MainActivity;
import com.eagerbeaver.eagerbeaver.R;
import com.eagerbeaver.eagerbeaver.models.SMSContact;
import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo;
import com.eagerbeaver.eagerbeaver.widgets.HollerZoneView;
import com.github.snowdream.android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class FragmentHollerZone extends FragmentBase implements OnClickListener {

	public static String PAGEID = "HOLLERING_ZONE";

	// constructor
	public FragmentHollerZone() {
		super();
		mStrPageID = PAGEID;
	}

	TotalStatsInfo mInfo;

	HollerZoneView mHZView;
	RelativeLayout holleringzone_view;
	
	FrameLayout holleringzone_intro;
	ImageView btn_intro_close;
	ImageButton btn_intro_show;
	
	LinearLayout btn_zone_player;
	LinearLayout btn_zone_thirsty;
	LinearLayout btn_zone_bored;
	LinearLayout btn_zone_hooked;
	
	TextView txt_contact_info;
	
	ImageButton hollering_btn_share;
	
	int canvas_scaled = 0; 
	int cur_region_id = 0;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.fragment_holler_zone, container, false);
		
		holleringzone_intro = (FrameLayout) view.findViewById(R.id.holleringzone_intro);
		holleringzone_view = (RelativeLayout) view.findViewById(R.id.holleringzone_view);
		
		btn_intro_close = (ImageView) view.findViewById(R.id.btn_intro_close);
		btn_intro_close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				holleringzone_intro.setVisibility(View.GONE);
			}
		});
		
		btn_intro_show = (ImageButton) view.findViewById(R.id.hollering_btn_info);
		btn_intro_show.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				holleringzone_intro.setVisibility(View.VISIBLE);
			}
		});
		
		mRootLayout = (RelativeLayout) view.findViewById(R.id.holleringzone_scale);		
		mInfo = ((BaseApplication) getActivity().getApplication()).getTotalStatsInfo();
		
		btn_zone_player = (LinearLayout) view.findViewById(R.id.btn_zone_player);
		btn_zone_thirsty = (LinearLayout) view.findViewById(R.id.btn_zone_thirsty);
		btn_zone_bored = (LinearLayout) view.findViewById(R.id.btn_zone_bored);
		btn_zone_hooked = (LinearLayout) view.findViewById(R.id.btn_zone_hooked);
		
		hollering_btn_share = (ImageButton) view.findViewById(R.id.hollering_btn_share);
		hollering_btn_share.setOnClickListener(this);
		
		txt_contact_info = (TextView) view.findViewById(R.id.txt_contact_info);
		txt_contact_info.setVisibility(View.GONE);
		
		showValues();
		
		return view;
	}
	
	/*
	public void updateViewPositions() {
		mHZView.setCanvasTopLeft(mHZView.getTop(), mHZView.getLeft());
	}
	*/
	
	public void showValues() {
		mHZView = new HollerZoneView(getActivity(), this);
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		
		mRootLayout.addView(mHZView, params);
		
		btn_zone_player.setOnClickListener(this);
		btn_zone_thirsty.setOnClickListener(this);
		btn_zone_bored.setOnClickListener(this);
		btn_zone_hooked.setOnClickListener(this);
		
		for(int i = 0; i < mInfo.m_ContactsInfo.size(); i++) {
			SMSContact contact = mInfo.m_ContactsInfo.get(i);
			
			Uri uri = contact.getPhotoUri(mMainActivity);
			
			mHZView.addInfo(contact.first_name, contact.getRatioX(), contact.getRatioY(), uri, contact.getRegionNumber());
		}
	}
	
	public void showContactText(String str) {
		if (str.length() == 0) {
			txt_contact_info.setVisibility(View.GONE);
		} else {
			txt_contact_info.setText(str);
			txt_contact_info.setVisibility(View.VISIBLE);
		}
	}
	

	Bitmap myBitmap;
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_zone_player:
			scaleCanvas(4);
			break;
		case R.id.btn_zone_thirsty:
			scaleCanvas(1);
			break;
		case R.id.btn_zone_bored:
			scaleCanvas(3);
			break;
		case R.id.btn_zone_hooked:
			scaleCanvas(2);
			break;
		case R.id.hollering_btn_share:
			View v1 = holleringzone_view;
			v1.setDrawingCacheEnabled(true);
			myBitmap = v1.getDrawingCache();
			saveBitmap(myBitmap);
			break;
		default:
			break;
		}
	}
	
	private void showShareActionSheet(String mPath) {
		Log.v("holleringzong showShareActionSheet path", mPath);
		Uri capture_img_uri = Uri.fromFile(new File(mPath));
		Intent shareIntent = new Intent();
		shareIntent.setAction(Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, "EagerBeaver");
		shareIntent.putExtra(Intent.EXTRA_STREAM, capture_img_uri);
		shareIntent.setType("image/jpeg");
		shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		
		startActivity(Intent.createChooser(shareIntent, "Share via"));
	}
	
	private void saveBitmap(Bitmap bitmap) {
		
		Date date = new Date();
		String filePath = Environment.getExternalStorageDirectory()
				+ File.separator + "Pictures/screenshot" + DateFormat.format("yyMMddhhmmss", date) + ".png";
		File imagePath = new File(filePath);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(imagePath);
			bitmap.compress(CompressFormat.PNG, 100, fos);
			fos.flush();
			fos.close();
			showShareActionSheet(filePath);
		} catch (FileNotFoundException e) {
			Log.e("Hollerzone saveBitmap Exception", e);
		} catch (IOException e) {
			Log.e("Hollerzone saveBitmap Exception", e);
		}
	}
	
	private void scaleCanvas(int region_id) {
		if (canvas_scaled == 1 && cur_region_id == region_id) {
			canvas_scaled = 0;
			mHZView.scaleCanvas(0);
		} else {
			canvas_scaled = 1;
			mHZView.scaleCanvas(region_id);
			cur_region_id = region_id;
		}
	}
}
