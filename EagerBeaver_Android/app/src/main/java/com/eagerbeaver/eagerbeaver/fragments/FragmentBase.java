package com.eagerbeaver.eagerbeaver.fragments;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.eagerbeaver.eagerbeaver.MainActivity;

public class FragmentBase extends Fragment {
	public String mStrPageID;
	public MainActivity mMainActivity;
	protected ViewGroup mRootLayout;
	
	public boolean mbFragmentLayoutDone = false;
	
	public void onStart() {
		super.onStart();
		
		
		if(mRootLayout != null) {
			mRootLayout.setDrawingCacheEnabled(true);
			
			ViewTreeObserver vto = mRootLayout.getViewTreeObserver(); 
			    vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() { 
			    @SuppressLint("NewApi")
				@SuppressWarnings("deprecation")
				@Override 
			    public void onGlobalLayout() {
					
			    	if (Build.VERSION.SDK_INT < 16) {
			    		mRootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			        } else {
			        	mRootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
			        }
			    	
			    	fragmentAppeared();
			    	mbFragmentLayoutDone = true;
			    } 
			}); 
		}
	}
	
	
	public void fragmentAppeared() {
		
	}
}
