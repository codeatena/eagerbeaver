package com.eagerbeaver.eagerbeaver.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.eagerbeaver.eagerbeaver.BaseApplication;
import com.eagerbeaver.eagerbeaver.MainActivity;
import com.eagerbeaver.eagerbeaver.R;
import com.eagerbeaver.eagerbeaver.models.SMSContact;
import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo;
import com.eagerbeaver.eagerbeaver.widgets.AvatarImageView;
import com.eagerbeaver.eagerbeaver.widgets.PhotoCircleView;
import com.github.snowdream.android.util.Log;

import java.util.ArrayList;
import java.util.Comparator;

public class FragmentHollering extends FragmentBase implements OnClickListener {

	public static String PAGEID = "HOLLERING";

	// constructor
	public FragmentHollering() {
		super();
		mStrPageID = PAGEID;
	}

	TotalStatsInfo mInfo;

	ListView mListHollering;
	ArrayList<SMSContact> mContactList;
	ContactArrayAdapter mContactAdapter;
	
	LinearLayout btn_my_peeps;
	ImageButton hollering_btn_search;
	EditText hollering_txt_search;
	
	private KeyListener searchKeyListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_hollering, container, false);

		mListHollering = (ListView) view.findViewById(R.id.lst_hollering);
		
		btn_my_peeps = (LinearLayout) view.findViewById(R.id.btn_my_peeps);
		btn_my_peeps.setOnClickListener(this);
		
		hollering_btn_search = (ImageButton) view.findViewById(R.id.hollering_btn_search);
		hollering_btn_search.setOnClickListener(this);
		
		hollering_txt_search = (EditText) view.findViewById(R.id.hollering_txt_search);
		hollering_txt_search.setVisibility(View.GONE);
		searchKeyListener = hollering_txt_search.getKeyListener();
		hollering_txt_search.setKeyListener(null);
		hollering_txt_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					// Hide soft keyboard
					hideKeyboard();
				}
			}
		});
		hollering_txt_search.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				
				// Hide when user touch enter key
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					hideKeyboard();
					return true;
				}
				return false;
			}
		});
		
		mRootLayout = view;

		updateListView(0);

		return view;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
		case R.id.btn_my_peeps:
			showFilterPopup(v);
			break;
		case R.id.hollering_btn_search:
			showKeyboard();
			break;
		default:
			break;
		}
		
	}
	
	@SuppressLint("NewApi")
	private void showFilterPopup(View v) {
		try {
			PopupMenu popupMenu = new PopupMenu(mMainActivity, v);
			popupMenu.setOnMenuItemClickListener(mMainActivity);
			popupMenu.inflate(R.menu.menu_peeps_filter);
			popupMenu.show();
		}
		catch(Exception e) {
			
		}
	}
	
	private void showKeyboard() {
		Animation anim = AnimationUtils.loadAnimation(mMainActivity, R.anim.abc_slide_in_bottom);
		hollering_txt_search.setVisibility(View.VISIBLE);
		hollering_txt_search.startAnimation(anim);
		
		// Restore key listener - this will make the filed editable again.
		hollering_txt_search.setKeyListener(searchKeyListener);
		// Focus the field.
		hollering_txt_search.requestFocus();
		// Show soft keyboard for the user to enter the value.
		InputMethodManager imm = (InputMethodManager) mMainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(hollering_txt_search, InputMethodManager.SHOW_IMPLICIT);
//		hollering_txt_search.setBackgroundColor(Color.WHITE);
	}
	
	private void hideKeyboard() {
//		hollering_txt_search.setBackgroundColor(Color.TRANSPARENT);
		InputMethodManager imm = (InputMethodManager) mMainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(hollering_txt_search.getWindowToken(), 0);
		// Make it non-editable again.
		hollering_txt_search.setKeyListener(null);
		
		// Make edittext color transparent if there is no text.
		if (hollering_txt_search.getText().length() == 0) {
			Animation anim = AnimationUtils.loadAnimation(mMainActivity, R.anim.abc_slide_out_bottom);
			hollering_txt_search.setVisibility(View.GONE);
			hollering_txt_search.startAnimation(anim);
			
//			hollering_txt_search.setBackgroundColor(Color.TRANSPARENT);
		}
	}

	public void updateListView(int filter_num) {
		
		// Make edittext color transparent if there is no text.
		if (hollering_txt_search.getText().length() != 0) {
			Animation anim = AnimationUtils.loadAnimation(mMainActivity, R.anim.abc_slide_out_bottom);
			hollering_txt_search.setVisibility(View.GONE);
			hollering_txt_search.startAnimation(anim);
			
			hollering_txt_search.setText("");
		}
		
		mInfo = ((BaseApplication) getActivity().getApplication()).getTotalStatsInfo();
		
		mContactList = new ArrayList<SMSContact>();
		for (int i = 0; i < mInfo.m_ContactsInfo.size(); i++) {
			SMSContact contact = mInfo.m_ContactsInfo.get(i);
			
			int getLastPeriod = contact.getLastTextedPeriod();
			
			Log.i("passed day: " + Integer.toString(getLastPeriod));
			switch (filter_num) {
				case 0:
					addContact(contact);
					break;
				case 1:
					if (getLastPeriod <= 15) {
						addContact(contact);
					}
					break;
				case 2:
					if (getLastPeriod <= 30) {
						addContact(contact);
					}
					break;
				case 3:
					if (contact.getRegionNumber().charAt(0) == '0') {									//Bored
						addContact(contact);
					}	
					break;
				case 4:
					if (contact.getRegionNumber().charAt(0) == '1') {									//Player
						addContact(contact);
					}
					break;
				case 5:
					if (contact.getRegionNumber().charAt(0) == '2') {									//Hooked
						addContact(contact);
					}
					break;
				case 6:
					if (contact.getRegionNumber().charAt(0) == '3') {									//Thirsty
						addContact(contact);
					}
					break;
				default: 
					break;
			}
		}
		
		mMainActivity.hideLoadingScreen();

		mContactAdapter = new ContactArrayAdapter(mMainActivity, R.layout.hollering_item, mContactList);
		
		// Sort list in alphabetical order.
		mContactAdapter.sort(new Comparator<SMSContact>() {

			@Override
			public int compare(SMSContact lhs, SMSContact rhs) {
				// TODO Auto-generated method stub
				return lhs.name.compareTo(rhs.name);
			}
		});
		mListHollering.setAdapter(mContactAdapter);
		mListHollering.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				FragmentHolleringDetail fragment = new FragmentHolleringDetail();

				SMSContact contact = mContactList.get(position);
				fragment.setValue(contact);

				mMainActivity.addPage(fragment);
			}
		});
		
		hollering_txt_search.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				mContactAdapter.getFilter().filter(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

	}

	private void addContact(SMSContact contact) {

		boolean added = false;
		float newScore = contact.getOverallScore();
		for (int i = 0; i < mContactList.size(); i++) {
			SMSContact oldContact = mContactList.get(i);
			float score = oldContact.getOverallScore();
			if (score < newScore) {
				mContactList.add(i, contact);
				added = true;
				break;
			}
		}
		if (added == false) {
			mContactList.add(contact);
		}
	}

	private class ContactArrayAdapter extends ArrayAdapter<SMSContact> {

		
		private ArrayList<SMSContact> filteredData = null;
		private ContactFilter mFilter = new ContactFilter();

		public ContactArrayAdapter(Context context, int textViewResourceId,
				ArrayList<SMSContact> objects) {
			super(context, textViewResourceId, objects);
			filteredData = objects;
		}

		public int getCount() {
			return filteredData.size();
		}
		
		public SMSContact getItem(int position) {
			return filteredData.get(position);
		}
		
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.hollering_item, parent, false);
			}

			PhotoCircleView img_avatar = (PhotoCircleView) convertView.findViewById(R.id.img_avatar);
			TextView txtName = (TextView) convertView.findViewById(R.id.txt_name);
			// TextView txtScoring = (TextView)
			// convertView.findViewById(R.id.txt_scoring);
//			TextView txtResponsiveness = (TextView) convertView.findViewById(R.id.txt_responsiveness);
			AvatarImageView img_p_avatar = (AvatarImageView) convertView.findViewById(R.id.img_p_avatar);

			SMSContact contact = filteredData.get(position);

			txtName.setText(contact.name);

			// Set AveLast Score background color
			/*
			GradientDrawable drawable = (GradientDrawable) txtResponsiveness.getBackground();
			if (avelast_score > 0) {
				txtResponsiveness.setText("+" + String.format("%.2f", avelast_score) + "%");
				drawable.setColor(getResources().getColor(R.color.color_green));
			} else {
				txtResponsiveness.setText("-" + String.format("%.2f", avelast_score) + "%");
				drawable.setColor(getResources().getColor(R.color.color_red));
			}
			*/

			// Get Contact photo url & Set ImageView
			Uri uri = contact.getPhotoUri(this.getContext());
			if (uri != null) {
				img_avatar.setImageData(uri);
			} else {
				img_avatar.setImageData(null);
//				img_avatar.setImageResource(R.drawable.ic_launcher);
			}

			// Set avatar image and lbl interest
			int avatar_id = 0;

			if (contact.getRegionNumber().charAt(0) == '0') {
				avatar_id = 0;											//Bored
			} else if (contact.getRegionNumber().charAt(0) == '1') {		
				avatar_id = 1;											//Player
			} else if (contact.getRegionNumber().charAt(0) == '2') {
				avatar_id = 2;											//Hooked
			} else {	
				avatar_id = 3;											//Thirsty
			}
			img_p_avatar.setAvatarId(avatar_id);
			
			return convertView;
		}
		
		public ContactFilter getFilter() {
			return mFilter;
		}
		
		private class ContactFilter extends Filter {

			@SuppressLint("DefaultLocale")
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				// TODO Auto-generated method stub
				String str_filter = constraint.toString().toLowerCase();
				
				FilterResults results = new FilterResults();
				
				int count = mContactList.size();
				final ArrayList<SMSContact> nList = new ArrayList<SMSContact>(count);
				
				SMSContact filter_item;
				
				for ( int i = 0; i < count; i++) {
					filter_item = mContactList.get(i);
					if (filter_item.name.toLowerCase().contains(str_filter)) {
						nList.add(filter_item);
					}
				}
				
				results.values = nList;
				results.count = nList.size();
				
				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// TODO Auto-generated method stub
				filteredData = (ArrayList<SMSContact>) results.values;
				notifyDataSetChanged();
			}
			
		}

	}

}
