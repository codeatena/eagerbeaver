
package com.eagerbeaver.eagerbeaver.widgets;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.widget.TextView;

import com.eagerbeaver.eagerbeaver.R;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.MarkerView;
import com.github.mikephil.charting.utils.Utils;

public class MyMarkerView extends MarkerView {

    private TextView txt_date;
    private TextView txt_your_time;
    private TextView txt_contact_time;
    
    private SimpleDateFormat dateFormat = null;

    public MyMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        txt_date = (TextView) findViewById(R.id.chart_bubble_date);
        txt_your_time = (TextView) findViewById(R.id.chart_bubble_your_time);
        txt_contact_time = (TextView) findViewById(R.id.chart_bubble_contact_time);
        
        if (dateFormat == null) {
        	dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        }
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content
    @Override
    public void refreshContent(Entry e, int dataSetIndex) {

    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DAY_OF_YEAR, - (7 - e.getXIndex()));
    	Date newDate = calendar.getTime();
    	
    	String date = dateFormat.format(newDate);
    	txt_date.setText(date);
    	
        if (e instanceof CandleEntry) {

            CandleEntry ce = (CandleEntry) e;
//            txt_date.setText("Apr 12, 2014" + Utils.formatNumber(ce.getHigh(), 0, true));
            if (dataSetIndex == 0) {
            	txt_your_time.setText("You: " + ce.getVal() + " min.");	
            } else {
            	txt_contact_time.setText("Contact: " + ce.getVal() + " min.");	
            }
        } else {
//        	txt_date.setText("Apr 12, 2014" + Utils.formatNumber(e.getVal(), 0, true));
        	if (dataSetIndex == 0) {
        		txt_your_time.setText("You: " + e.getVal() + " min.");
        	} else {
        		txt_contact_time.setText("Contact: " + e.getVal() + " min.");	
        	}
        }
        
        this.setOffsets(-this.getMeasuredWidth() / 2, -20);
    }
}
