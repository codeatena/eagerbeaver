package com.eagerbeaver.eagerbeaver.widgets;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract.Contacts;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

/**
 * Draws the photo in a circle frame
 */
public class PhotoCircleView extends ImageView {

	private Uri bmpUri;

	public PhotoCircleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
			setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}
	}

	public void setImageData(Uri uri) {
		bmpUri = uri;
		invalidate();
	}

	Path path = new Path();
	RectF rectClip = new RectF();
	Rect rectSrc = new Rect();
	Rect rectDst = new Rect();
	Paint paint = new Paint();

	public void onDraw(Canvas canvas) {
		int width = getWidth();
		int height = getHeight();
		// draw the photo
		{
			Bitmap bitmap;
//			if (getDrawable() != null) {
//				bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
//			} else {
//				bitmap = getContactBitmapFromURI(getContext(), bmpUri);
//			}
			if (bmpUri != null) {
				bitmap = getContactBitmapFromURI(getContext(), bmpUri);
			} else {
				bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
			}

			if (bitmap != null) {
				bitmap = createCircleBitMap(bitmap);
				rectSrc.left = 0;
				rectSrc.top = 0;
				rectSrc.right = bitmap.getWidth();
				rectSrc.bottom = bitmap.getHeight();
				rectDst.left = 0;
				rectDst.top = 0;
				rectDst.right = width;
				rectDst.bottom = height;
				canvas.drawBitmap(bitmap, rectSrc, rectDst, paint);
			} else {
				paint.setColor(0xff0000ff);
				paint.setStyle(Style.STROKE);
				canvas.drawOval(rectClip, paint);
			}
			if (getDrawable() == null && bitmap != null) {
				bitmap.recycle();
			}
		}
	}
	
	private Bitmap createCircleBitMap(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
	            bitmap.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
	            bitmap.getWidth() / 2, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);
	    
	    return output;
    }

	public static Bitmap getContactBitmapFromURI(Context context, Uri uri) {
		InputStream input = null;
		try {
			input = context.getContentResolver().openInputStream(uri);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (input == null) {
			return null;
		}
		Bitmap bmp = BitmapFactory.decodeStream(input);
		try {
			input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bmp;
	}
}
