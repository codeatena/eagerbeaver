package com.eagerbeaver.eagerbeaver.models;

import android.content.Context;
import android.database.Cursor;

import com.eagerbeaver.eagerbeaver.utils.Utils;
import com.github.snowdream.android.util.Log;

import java.util.ArrayList;
import java.util.Date;

public class MessageModel {
	
	public long _id;
	
	//The thread ID of the message
	public int thread_id;
	
	//The address of the other party
	public String address;
	
	//The person ID of the sender
	//OR
	//The id of the sender of the conversation, if present
	//Type: INTEGER (reference to item in content://contacts/people)
	public long person;
	
	//The date the message was sent
	public Date date;
	
	//The protocol identifier code
	public boolean incoming;

	//The subject of the message, if present
	public String subject;
		
	//The body of the message
	public String body;
		
	//if it is mms or sms
	public boolean sms;
	
	public ArrayList<String> arrNumbers;
	
	public static MessageModel create(Context ctx, Cursor cursor, boolean isSMS) {
		MessageModel model = new MessageModel();
		
		model._id = cursor.getLong(cursor.getColumnIndex("_id"));
		
		model.sms = isSMS;

		model.thread_id = cursor.getInt(cursor.getColumnIndex("thread_id"));
		
		if(model.sms) {
			
			String strdate = cursor.getString(cursor.getColumnIndex("date"));
			model.date = Utils.getDateFromSMSDate(strdate);
			
			int idxAddr = cursor.getColumnIndex("address");
            model.address = cursor.getString(idxAddr);

            if (model.address == null) {
                return null;
            }

			model.address = model.address.replace(" ", "").replace("+", "").replace("-", "").replace("(", "").replace(")", "");
			
			//protocol doesn't work insome of devices, need to use type
//			if(cursor.getType(cursor.getColumnIndex("protocol")) == Cursor.FIELD_TYPE_NULL) {
//				model.incoming = false;
//			}
//			else {
//				String protocol = cursor.getString(cursor.getColumnIndex("protocol"));
//				model.incoming = (protocol != null);
//			}
			
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			
			if (type == 1) {
				//received
				model.incoming = true;
			} else if (type == 2) {
				model.incoming = false;
			} else {
				return null;
			}
			
			model.body = cursor.getString(cursor.getColumnIndex("body"));
			model.subject = cursor.getString(cursor.getColumnIndex("subject"));
			
			Log.i("sms data: " + model.date.toString() + " " + model.address + " " + model.incoming + " " + model.body);

		} else {
			
			String strdate = cursor.getString(cursor.getColumnIndex("date"));
			model.date = Utils.getDateFromSMSDateSec(strdate);
				
			model.arrNumbers = Utils.getAddressNumbers(ctx, model._id);

			if (model.arrNumbers.size() == 0) {
				model.address = null;
				return null;
			} else if (model.arrNumbers.size() == 1) {
				model.address = model.arrNumbers.get(0);
				model.incoming = false;
			} else if (model.arrNumbers.size() == 2) {
				model.address = model.arrNumbers.get(0);
				model.incoming = true;
			} else {
				model.address = null;
				return null;
			}

            Log.i("mms data: " + model.date.toString() + " " + Utils.getNumbersString(model.arrNumbers) + " " + model.incoming);

            model.body = "";
		}
		
		return model;
	}
	
	public MessageModel() {
		
	}
}
