package com.eagerbeaver.eagerbeaver.fragments;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eagerbeaver.eagerbeaver.MainActivity;
import com.eagerbeaver.eagerbeaver.R;
import com.eagerbeaver.eagerbeaver.utils.SMSContentProvider;
import com.eagerbeaver.eagerbeaver.utils.Utils;

public class FragmentSMSContent extends FragmentBase {
	
	public static String PAGEID = "SMSContent";
	
	//constructor
	public FragmentSMSContent() {
		super();
		mStrPageID = PAGEID;
	}

	public Uri mContentUri = Uri.parse("content://sms");
	public String[] mProjection;
	public void setUri(Uri uri, String[] projection) {
		mContentUri = uri;
		mProjection = projection;
	}
	
	public ListView mListView;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		RelativeLayout view = (RelativeLayout)inflater.inflate(R.layout.fragment_smscontent, container, false);
		
		mListView = (ListView)view.findViewById(R.id.list_sms_content);
		
		mListView.setAdapter(new SMSAdapter(getActivity(), SMSContentProvider.getContent(getActivity(), mContentUri, mProjection)));
		
		mRootLayout = view;
		return view;
	}
	
	
	
	public class SMSAdapter extends CursorAdapter {
		public TextView pname;
		Context context;

		@SuppressWarnings("deprecation")
		public SMSAdapter(Context context, Cursor c) {
		    super(context, c);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		    View retView = inflater.inflate(R.layout.sms_content_item, parent, false);
		    return retView;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			String string = Utils.getStringRepContentFromCursor(cursor);
			((TextView)view.findViewById(R.id.txt_content)).setText(string);
		}
	}
	
}
