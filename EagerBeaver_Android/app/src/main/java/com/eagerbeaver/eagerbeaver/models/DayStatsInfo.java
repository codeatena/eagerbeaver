package com.eagerbeaver.eagerbeaver.models;

import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo.MsgDirectionType;

public class DayStatsInfo {
	public int numberOfMessages;
	public int numberOfOutgoingMsgs;
	public int numberOfIncomingMsgs;
	
	public int numberOfMMS;
	public int numberOfOutgoingMMS;
	public int numberOfIncomingMMS;
	
	public long totalLength;
	public long totalLengthIn;
	public long totalLengthOut;
	
	public long m_totalResponsesGet;
	public long m_totalResponsesGetTime;
	
	public long m_totalResponds;
	public long m_totalRespondsTime;
	
	public long m_totalMsgsEligibleForTime;
	public long m_totalBetweenTime;
	
	
	public static enum MsgMode {
		MsgModeTotal,
		MsgModeSend,
		MsgModeRecv
	}
	
	public void processMessage(boolean coming, boolean mms, int length, MsgDirectionType type, long time) {
		numberOfMessages++;
		
		if(coming) {
			numberOfIncomingMsgs++;
		}
		else {
			numberOfOutgoingMsgs++;
		}
		
		if(mms) {
			numberOfMMS++;
			if(!coming) {
				numberOfOutgoingMMS++;
			}
			else {
				numberOfIncomingMMS++;
			}
		}
		
		totalLength += length;
		if(coming) {
			totalLengthIn += length;
		}
		else {
			totalLengthOut += length;
		}
		
		if(time < 24 * 60 * 60) {
			if(type == MsgDirectionType.MsgDirectionIn) {
				m_totalResponsesGet++;
				m_totalResponsesGetTime += time;
			}
			else if(type == MsgDirectionType.MsgDirectionOut) {
				m_totalResponds ++;
				m_totalRespondsTime += time;
			}
		}
	}
	
	public int getAveTime() {
		if(m_totalResponsesGet + m_totalResponds == 0)
			return 0;
		return (int)((m_totalResponsesGetTime + m_totalRespondsTime) / (m_totalResponsesGet + m_totalResponds)); 
	}
	
	public int getAveResponseTime()  {
		if(m_totalResponsesGet == 0) {
			return 0;
		}
		
		return (int)(m_totalResponsesGetTime / m_totalResponsesGet);
	}
	
	public int getAveRespondTime()  {
		if(m_totalResponds == 0) {
			return 0;
		}
		return (int)(m_totalRespondsTime / m_totalResponds);
	}
	
	
	public int getNumberOfMessages(MsgMode mode) {
		int numMsg = numberOfMessages;
		if(mode == MsgMode.MsgModeRecv) {
			numMsg = numberOfIncomingMsgs;
		} else if(mode == MsgMode.MsgModeSend) {
			numMsg = numberOfMessages - numberOfIncomingMsgs;
		}
		return numMsg;
	}
	
	
	public static float calculateTrend(float values[], int count) {
		int mid = count / 2;
		
		float avePast  = 0;
		for(int i = 0; i < mid; i++) {
			avePast += values[i];
		}
		avePast = avePast / mid;
		
		float avePrior = 0;
		for(int i = mid; i < count; i++) {
			avePrior += values[i];
		}
		avePrior = avePrior / mid;
		
		return avePast == 0 ? 0 : (avePast - avePrior) / avePast * 100;
	}
	
	
	public static float getScore(long charlength, int responsetime, int messages) {
		float res;
		res = (charlength * 20 + responsetime * 45 / 60 + messages * 35) / 100;
		return res;
	}
	
	public float getScoreStat() {
		return getScore(numberOfMessages == 0 ? 0 : totalLength / numberOfMessages, getAveTime(), getNumberOfMessages(MsgMode.MsgModeRecv));
	}
}
