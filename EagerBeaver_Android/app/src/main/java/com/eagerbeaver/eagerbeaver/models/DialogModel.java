package com.eagerbeaver.eagerbeaver.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DialogModel {
	
	public int thread_id;
	ArrayList<MessageModel> msgs;
	
	public DialogModel(int _thread_id) {
		msgs = new ArrayList<MessageModel>();
		thread_id = _thread_id;		
	}
	
	public void sortbyDate() {
		Collections.sort(msgs, new Comparator<MessageModel>() {

			@Override
			public int compare(MessageModel lhs, MessageModel rhs) {
				// TODO Auto-generated method stub
				
				return lhs.date.compareTo(rhs.date);
			}
		});
	}
}