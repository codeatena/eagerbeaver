package com.eagerbeaver.eagerbeaver.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

public class SMSContentProvider {
	
	
	public static Cursor getContent(Context context, Uri uri, String[] projection) {
		// Create Inbox box URI
		ContentResolver cr = context.getContentResolver();
		// Fetch Inbox SMS Message from Built-in Content Provider
		Cursor c = cr.query(uri, projection, null, null, null);
	    return c;
	}
	
	public static Cursor getContent(Context context, Uri uri) {
	    return getContent(context, uri, null);
	}
	
	public static Cursor getAllSMSMessages(Context context) {
		return getContent(context, Uri.parse("content://sms"));
	}
	
	public static Cursor getAllMMSMessages(Context context) {
		return getContent(context, Uri.parse("content://mms"));
	}
	
	public static Cursor getAllContacts(Context context) {
		return getContent(context, ContactsContract.Contacts.CONTENT_URI);
	}
	
	public static Cursor getAllConversations(Context context) {
		return getContent(context, Uri.parse("content://mms-sms/conversations?simple=true"));
	}
}