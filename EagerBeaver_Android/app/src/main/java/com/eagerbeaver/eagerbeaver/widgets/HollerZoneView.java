package com.eagerbeaver.eagerbeaver.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.eagerbeaver.eagerbeaver.R;
import com.eagerbeaver.eagerbeaver.fragments.FragmentHollerZone;
import com.eagerbeaver.eagerbeaver.utils.Utils;
import com.github.snowdream.android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class HollerZoneView extends View{
	
	int m_xMin;
	int m_xMax;
	int m_yMax;
	
	Rect rectSrc = new Rect();
	Rect rectDst = new Rect();
	
	private float mScaleFactor = 1.f;
	
	private int scale_region_id = 0;
	private float mPosX;
	private float mPosY;
	
	static int zone_bmpWidth = 60;
	static int zone_bmpHeight = 60;
	
	LayoutInflater mInflater;
	
	static View bubble_view = null;
	TextView txt_contact_name = null;
	
    private static final int INVALID_POINTER_ID = -1;
    
	private ScaleGestureDetector mScaleDetector;

	private float mLastTouchX;
	private float mLastTouchY;
	private int mActivePointerId = INVALID_POINTER_ID;
	
	FragmentHollerZone fragmentHollerZone;

	public HollerZoneView(Context context, FragmentHollerZone fragment) {
		super(context);
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
		fragmentHollerZone = fragment;

		customInit();
//		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater = LayoutInflater.from(context);
		
		bubble_view = mInflater.inflate(R.layout.hollering_zone_name, null);
		txt_contact_name = (TextView) bubble_view.findViewById(R.id.txt_contact_name);
	}
	
	private class ScaleListener 
	    extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
		    mScaleFactor *= detector.getScaleFactor();
		
		    // Don't let the object get too small or too large.
		    mScaleFactor = Math.max(1.f, Math.min(mScaleFactor, 1.6f));
		
		    invalidate();
		    return true;
		}
	}
	
	static private float canvas_top = 0;
	static private float canvas_left = 0;
	
	public void setCanvasTopLeft(float left, float top) {
		canvas_left = left;
		canvas_top = top;
	}
	/*
	@Override
	public void onWindowFocusChanged( boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			canvas_left = this.getLeft();
			canvas_top = this.getTop();
		}
	}
	*/
	private float view_width;
	private float view_height;
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
	    // Let the ScaleGestureDetector inspect all events.
	    mScaleDetector.onTouchEvent(ev);
	    
	    view_width = getWidth();
		view_height = getHeight();
		
		if (canvas_left == 0 || canvas_top == 0) {
			int test1[] = new int[2];
			this.getLocationOnScreen(test1);
//			Log.v("view loc", "hollering view loc: " + test1[0] + "," + test1[1]);
			setCanvasTopLeft(test1[0], test1[1]);
		}
	    
	    final int action = ev.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN: {
	            final float x = ev.getX();
	            final float y = ev.getY();
	            
//	            Log.v("x * y :", x + " * " + y);
	
	            mLastTouchX = x;
	            mLastTouchY = y;
	            mActivePointerId = ev.getPointerId(0);
	            
	            fragmentHollerZone.showContactText("");
	            
	            //Check if the x and y position of the touch is inside the bitmap
	        	if (mInfos_canvas != null) {
	    			for(int i = 0; i < mInfos_canvas.size(); i++) {
	    				ZoneInfo info = mInfos_canvas.get(i);
	    				
	    				float abs_canvas_x = info.canvas_x + view_width / 2;
	    				float abs_canvas_y = info.canvas_y + view_height / 2;
	    				
	    				float abs_x = mLastTouchX;
	    				float abs_y = mLastTouchY;
	    				
	    				if( scale_region_id == 1) {						// Region Thirsty
	    					abs_x = (abs_x + view_width * (mScaleFactor - 1)) ;
	    				} else if (scale_region_id == 2) {				// Region Hooked
	    					abs_x = (abs_x + view_width * (mScaleFactor - 1)) ;
	    					abs_y = (abs_y + view_height * (mScaleFactor - 1)) ;
	    				} else if (scale_region_id == 3) {				// Region Bored
	    					abs_y = (abs_y + view_height * (mScaleFactor - 1)) ;
	    				}
	    				
				        if( abs_x > (abs_canvas_x - zone_bmpWidth / 2) * mScaleFactor && abs_x < (abs_canvas_x + zone_bmpWidth / 2) * mScaleFactor 
				        		&& abs_y > (abs_canvas_y - zone_bmpHeight / 2) * mScaleFactor && abs_y < (abs_canvas_y + zone_bmpHeight / 2) * mScaleFactor )
				        {
				            //Bitmap touched
	    					Log.v("ss: ", "clicked");
	    					mLastTouchX = abs_canvas_x * mScaleFactor;
	    					mLastTouchY = abs_canvas_y * mScaleFactor;
	    					
	    					if( scale_region_id == 1) {						// Region Thirsty
	    						mLastTouchX -= view_width * (mScaleFactor - 1);
		    				} else if (scale_region_id == 2) {				// Region Hooked
		    					mLastTouchX -= view_width * (mScaleFactor - 1);
		    					mLastTouchY -= view_height * (mScaleFactor - 1);
		    				} else if (scale_region_id == 3) {				// Region Bored
		    					mLastTouchY -= view_height * (mScaleFactor - 1);
		    				}
	    					
				        	txt_contact_name.setText(info.strName);
				        	showInterestInfoToast(info.strName, info.canvas_region_id);
				        	showBubbleAtLocation();
				        }
	    			}
	        	}
	        	
	            break;
	        }
        }
	    
	    return true;
	}
	
	/*
	static int msg_count_bored_01 = 3;
	static int msg_count_bored_02 = 3;
	static int msg_count_bored_03 = 3;
	static int msg_count_player_01 = 3;
	static int msg_count_player_02 = 3;
	static int msg_count_player_03 = 3;
	static int msg_count_hooked_01 = 3;
	static int msg_count_hooked_02 = 3;
	static int msg_count_hooked_03 = 3;
	static int msg_count_thirsty_01 = 3;
	static int msg_count_thirsty_02 = 3;
	static int msg_count_thirsty_03 = 3;
	*/
	

	private void showInterestInfoToast(String name, String region_id) {
		String text = Utils.getSuggestionMessage(name, region_id);
		fragmentHollerZone.showContactText(text);
	}
	
	

	public HollerZoneView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		customInit();
	}
	
	public HollerZoneView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		customInit();
	}
	
	
	Paint paint;
	
	public void customInit() {
		paint = new Paint();
		
		paint.setAntiAlias(true);
        paint.setStrokeWidth(5f);
        paint.setColor(getResources().getColor(R.color.color_bored));
        //paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        
	}
	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		canvas.save();
		
	    canvas.scale(mScaleFactor, mScaleFactor);
	    
		canvas.drawColor(Color.TRANSPARENT);
		
		int width = getMeasuredWidth();
		int height = getMeasuredHeight();
		
		int cx = width / 2;
		int cy = height / 2;
		
		mPosX = cx;
		mPosY = cy;
		
		if (scale_region_id == 1) {
			mPosX -= cx * mScaleFactor / 2;
		} else if (scale_region_id == 2) {
			mPosX -= cx * mScaleFactor / 2;
			mPosY -= cy * mScaleFactor / 2;
		} else if (scale_region_id == 3) {
			mPosY -= cy * mScaleFactor / 2;
		}
		
		canvas.translate(mPosX, mPosY);
		
		//cross hair drawing
		int AHS = cx / 30;
		canvas.drawLine(-cx, 0, cx, 0, paint);
		canvas.drawLine(-cx, 0, -cx + AHS, -AHS, paint);
		canvas.drawLine(-cx, 0, -cx + AHS, +AHS, paint);
		canvas.drawLine(+cx, 0, +cx - AHS, -AHS, paint);
		canvas.drawLine(+cx, 0, +cx - AHS, +AHS, paint);
		
		
		canvas.drawLine(0, -cy, 0, cy, paint);
		canvas.drawLine(0, -cy, AHS, -cy + AHS, paint);
		canvas.drawLine(0, -cy, - AHS, -cy + AHS, paint);
		canvas.drawLine(0,  cy, AHS, cy - AHS, paint);
		canvas.drawLine(0,  cy, -AHS, cy - AHS, paint);

//		canvas.restore();
		
		//pinpoint items
		
//		int centerX = (m_xMax + m_xMin) / 2;
//		float rangeX = ((float)(m_xMax - centerX) * 1.3f);
//		float rangeY = ((float)m_yMax * 1.5f);
		
		cx = cx - zone_bmpWidth;			// 60 is padding
		cy = cy - zone_bmpHeight;
		
		if (mInfos != null) {
			for(int i = 0; i < mInfos.size(); i++) {
				ZoneInfo info = mInfos.get(i);
				
//				float x = rangeX == 0 ? 0 : ( (info.ratio_x - centerX) * cx / rangeX );
//				float y = rangeY == 0 ? 0 : ( info.ratio_y * cy / rangeY );
				
				float x = -cx + 2 * cx * info.ratio_x / 100;
				float y = cy - 2 * cy * info.ratio_y / 100;
				
				// Save info canvas x, and canvas y
				info.setCanvasXY(x, y);
				
				if (mInfos_canvas == null) {
					mInfos_canvas = new ArrayList<ZoneInfo>(); 
				}
				// Add info to new array
				mInfos_canvas.add(info);
				
				Bitmap bmp;
				
				if(info.contact_uri != null) {
					bmp = getContactBitmapFromURI(getContext(), info.contact_uri);	
				} else {
					bmp = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_launcher);
				}
				
				if (bmp != null) {
					
					bmp = createCircleBitMap(bmp);
					
					width = bmp.getWidth();
					height = bmp.getHeight();
					
					Log.v("x, y: ", x + " * " + y);
					
					x = x - zone_bmpWidth / 2;
					y = y - zone_bmpHeight / 2;
					
					rectSrc.left = 0;
					rectSrc.top = 0;
					rectSrc.right = width;
					rectSrc.bottom = height;
					rectDst.left = (int)x;
					rectDst.top = (int)y;
					rectDst.right = (int)x + zone_bmpWidth;
					rectDst.bottom = (int)y + zone_bmpHeight;
					
					canvas.drawBitmap(bmp, rectSrc, rectDst, null);
					bmp.recycle();
					
				} else {
					canvas.drawCircle(x, y, 4, paint);	
				}
				
				paint.setTextAlign(Align.CENTER);
				//canvas.drawText(info.strName, x, y + TM, paint);
			}	
			
		}
		canvas.restore();
	}
	
	private Bitmap createCircleBitMap(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
	            bitmap.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
	            bitmap.getWidth() / 2, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);
	    
	    return output;
    }
	
	public static Bitmap getContactBitmapFromURI(Context context, Uri uri) {
		InputStream input = null;
		try {
			input = context.getContentResolver().openInputStream(uri);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (input == null) {
			return null;
		}
		Bitmap bmp = BitmapFactory.decodeStream(input);
		try {
			input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bmp;
	}
	
	public void scaleCanvas(int region_id) {
		
		scale_region_id = region_id;
		
		if (region_id == 0) {
			mScaleFactor = 1.f;
		} else {
			mScaleFactor = 1.6f;
		}
		
		invalidate();
	}
	
	public class ZoneInfo {
		String strName;
		int ratio_x;
		int ratio_y;
		Uri contact_uri;
		
		float canvas_x;
		float canvas_y;
		String canvas_region_id;
		
		public ZoneInfo(String name, int x, int y, Uri uri, String r_id) {
			strName = name;
			ratio_x = x;
			ratio_y = y;
			contact_uri = uri;
			canvas_region_id = r_id;
		}
		
		public void setCanvasXY(float x, float y) {
			canvas_x = x;
			canvas_y = y;
		}
		
		public void setCanvasRegionNum(String id) {
			canvas_region_id = id;
		}
	}
	
	ArrayList<ZoneInfo> mInfos;
	ArrayList<ZoneInfo> mInfos_canvas;
	
	public void reset() {
		if(mInfos == null)
			mInfos = new ArrayList<ZoneInfo>();
		else 
			mInfos.clear();
	}
	
	public void addInfo(String name, int x, int y, Uri uri, String regionNumber) {
		if(mInfos == null)
			reset();
		mInfos.add(new ZoneInfo(name, x, y, uri, regionNumber));
		
		m_xMax = 0;
		m_xMin = 100000;
		m_yMax = 0;
		
		for(int i = 0; i < mInfos.size(); i++) {
			ZoneInfo info = mInfos.get(i);
			
			int timeBetweenAve = info.ratio_x;
			if(m_xMax < timeBetweenAve) {
				m_xMax = timeBetweenAve;
			}
			if(m_xMin > timeBetweenAve) {
				m_xMin = timeBetweenAve;
			}
			
			int nConversation = info.ratio_y;
			if(m_yMax < Math.abs(nConversation)) {
				m_yMax = Math.abs(nConversation);
			}
		}
	}
	

	static PopupWindow bubble_popup = null;
	
	private void showBubbleAtLocation() {

        Log.e("showBubbleAtLocation");

		bubble_popup = new PopupWindow(this.getContext());
    	bubble_popup.setContentView(bubble_view);

		// Set content width and height
		bubble_popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		bubble_popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);

		// Closes the popup window when touch outside of it - when looses focus
		bubble_popup.setOutsideTouchable(true);
		bubble_popup.setFocusable(true);

		// Show anchored to button
		bubble_popup.setBackgroundDrawable(new ColorDrawable(this.getContext().getResources().getColor(android.R.color.transparent)));

//        bubble_view.measure(MeasureSpec.makeMeasureSpec(0,  MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0,  MeasureSpec.UNSPECIFIED));
        int w = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		int h = MeasureSpec.makeMeasureSpec(0,  MeasureSpec.UNSPECIFIED);

//        bubble_view.measure(w, h);

		int width = bubble_view.getMeasuredWidth();
		
		int scale_x = 0;
		int scale_y = (int)(zone_bmpHeight * mScaleFactor / 2);
		
		if (scale_region_id == 1 || scale_region_id == 2 ) {
			scale_x = (int)(zone_bmpHeight * (mScaleFactor - 1) / 2);
		}
		
		if (scale_region_id == 2 || scale_region_id == 3) {
			scale_y = (int)(zone_bmpHeight * (mScaleFactor - 1) / 2);
		}
		
		try {
			bubble_popup.showAtLocation(this, Gravity.NO_GRAVITY, (int)(mLastTouchX - width / 2 - scale_x + canvas_left), (int)(mLastTouchY + scale_y + canvas_top));	
		} catch(Exception e) {
			Log.e("showBubbleAtLocation exception", e);
		}
	}
}
