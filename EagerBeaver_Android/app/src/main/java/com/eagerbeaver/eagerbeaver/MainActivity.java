package com.eagerbeaver.eagerbeaver;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu.OnMenuItemClickListener;

import com.crittercism.app.Crittercism;
import com.eagerbeaver.eagerbeaver.analyzers.MSGAnalyzer;
import com.eagerbeaver.eagerbeaver.fragments.FragmentBase;
import com.eagerbeaver.eagerbeaver.fragments.FragmentHollerZone;
import com.eagerbeaver.eagerbeaver.fragments.FragmentHollering;
import com.eagerbeaver.eagerbeaver.fragments.FragmentSettings;
import com.eagerbeaver.eagerbeaver.fragments.FragmentTopStats;
import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo;
import com.eagerbeaver.eagerbeaver.utils.Utils;
import com.github.snowdream.android.util.FilePathGenerator;
import com.github.snowdream.android.util.Log;

import java.io.File;

@SuppressLint("NewApi")
public class MainActivity extends ActionBarActivity implements OnClickListener, OnMenuItemClickListener {

    public static final String LOG_TAG = "EagerBeaver";

	ViewPager mViewPager;
	MyViewPagerAdapter mSectionsPagerAdapter;
	
	private ImageButton tab_btn_peeps;
	private ImageButton tab_btn_zone;
	private ImageButton tab_btn_overview;
	private ImageButton tab_btn_settings;
	
	LinearLayout loading_screen;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Crittercism.initialize(getApplicationContext(), "54fd4e93e0697fa449637464");

        Log.setEnabled(true);
        Log.setLog2ConsoleEnabled(true);
        Log.setLog2FileEnabled(true);
        Log.setGlobalTag(LOG_TAG);
        Log.setFilePathGenerator(new FilePathGenerator.DefaultFilePathGenerator(Environment.getExternalStorageDirectory().getPath() + "/" + LOG_TAG + "/", LOG_TAG, ".txt"));

        createLogFile();

        Utils.setSpUnit(this, Utils.getScreenWidth(this) / 400.0);
		
		setContentView(R.layout.activity_main);
		
		tab_btn_peeps = (ImageButton) this.findViewById(R.id.tab_btn_peeps);
		tab_btn_zone = (ImageButton) this.findViewById(R.id.tab_btn_zone);
		tab_btn_overview = (ImageButton) this.findViewById(R.id.tab_btn_overview);
		tab_btn_settings = (ImageButton) this.findViewById(R.id.tab_btn_settings);
		
		tab_btn_peeps.setOnClickListener(this);
		tab_btn_zone.setOnClickListener(this);
		tab_btn_overview.setOnClickListener(this);
		tab_btn_settings.setOnClickListener(this);
		
		mViewPager = (ViewPager)findViewById(R.id.pager);
		
		loading_screen = (LinearLayout) this.findViewById(R.id.loading_screen);
		
		//initializeTabBar();
		(new MessageAnalysisTask()).execute();
		
		tab_btn_peeps.performClick();
	}

    private void createLogFile() {

        Log.i("createLogFile");

        try {
            File root = new File(Environment.getExternalStorageDirectory(), LOG_TAG);
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, LOG_TAG + ".txt");
            if (gpxfile.exists()) {
                gpxfile.delete();
            }
            if (!gpxfile.exists()) {
                gpxfile.createNewFile();
            }
        } catch (Exception e) {
            Log.e("createLogFileException", e);
        }
    }
	
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.tab_btn_peeps:
			tabButtonClickedAction(0);
			break;
		case R.id.tab_btn_zone:
			tabButtonClickedAction(1);
			break;
		case R.id.tab_btn_overview:
			tabButtonClickedAction(2);
			break;
		case R.id.tab_btn_settings:
			tabButtonClickedAction(3);
			break;
		default:
			break;
		}
	}
	
	public void tabButtonClickedAction(int position) {
		switch(position) {
		case 0:
			tab_btn_peeps.setBackgroundColor(this.getResources().getColor(R.color.tab_sel_bg));
			tab_btn_zone.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_overview.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_settings.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			
			tab_btn_peeps.setImageResource(R.drawable.tab_peeps_on);
			tab_btn_zone.setImageResource(R.drawable.tab_zone_off);
			tab_btn_overview.setImageResource(R.drawable.tab_overview_off);
			tab_btn_settings.setImageResource(R.drawable.tab_settings_off);
			
			break;
		case 1:
			tab_btn_peeps.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_zone.setBackgroundColor(this.getResources().getColor(R.color.tab_sel_bg));
			tab_btn_overview.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_settings.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			
			tab_btn_peeps.setImageResource(R.drawable.tab_peeps_off);
			tab_btn_zone.setImageResource(R.drawable.tab_zone_on);
			tab_btn_overview.setImageResource(R.drawable.tab_overview_off);
			tab_btn_settings.setImageResource(R.drawable.tab_settings_off);
			
			break;
		case 2:
			tab_btn_peeps.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_zone.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_overview.setBackgroundColor(this.getResources().getColor(R.color.tab_sel_bg));
			tab_btn_settings.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			
			tab_btn_peeps.setImageResource(R.drawable.tab_peeps_off);
			tab_btn_zone.setImageResource(R.drawable.tab_zone_off);
			tab_btn_overview.setImageResource(R.drawable.tab_overview_on);
			tab_btn_settings.setImageResource(R.drawable.tab_settings_off);
			break;
		case 3:
			tab_btn_peeps.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_zone.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_overview.setBackgroundColor(this.getResources().getColor(R.color.tab_bg));
			tab_btn_settings.setBackgroundColor(this.getResources().getColor(R.color.tab_sel_bg));
			
			tab_btn_peeps.setImageResource(R.drawable.tab_peeps_off);
			tab_btn_zone.setImageResource(R.drawable.tab_zone_off);
			tab_btn_overview.setImageResource(R.drawable.tab_overview_off);
			tab_btn_settings.setImageResource(R.drawable.tab_settings_on);
			break;
		default:
			break;
		}
		mViewPager.setCurrentItem(position);
		popLastPage();
	}
	
	/*
	@Override
	public void onWindowFocusChanged( boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
			if (mViewPager.getCurrentItem() == 0 && page != null && page.getClass() == FragmentHollerZone.class) {
				((FragmentHollerZone)page).updateViewPositions();	
			}
		}
	}
	*/
	
	private void initMainActivity() {
		
		mSectionsPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager()); 
		mViewPager.setAdapter(mSectionsPagerAdapter);
		
		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				tabButtonClickedAction(position);
			}
		});
	}
	
	public void showLoadingScreen() {
		loading_screen.setVisibility(View.VISIBLE);
	}
	
	public void hideLoadingScreen() {
		loading_screen.setVisibility(View.GONE);
	}
	
	class MyViewPagerAdapter extends FragmentPagerAdapter {

		public MyViewPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public Fragment getItem(int pos) {
			Fragment newFrag = null;
			switch(pos) { 
				case 0: {
					newFrag = new FragmentHollering();
					break;
				}
				case 1: {
					newFrag = new FragmentHollerZone();
					break;
				}
				case 2: {
					newFrag = new FragmentTopStats();
					break;
				}
				case 3: {
					newFrag = new FragmentSettings();
					break;
				}
			}
			return newFrag;
		}

		public int getPageIcon(int position) {
			switch (position) {
			case 0:
				return R.drawable.tab_peeps_on;
			case 1:
				return R.drawable.tab_zone_on;
			case 2:
				return R.drawable.tab_overview_on;
			case 3:
				return R.drawable.tab_settings_on;
			}
			return 0;
		}
	}

	private class MessageAnalysisTask extends AsyncTask<Void, Integer, Long> {
	     protected Long doInBackground(Void... params) {
	    	 
	         TotalStatsInfo info = MSGAnalyzer.analyzeTopStats(MainActivity.this);
	         ((BaseApplication)getApplication()).setTotalStatsInfo(info);
	         return 1l;
	     }

	     protected void onProgressUpdate(Integer... progress) {
	         
	     }

	     protected void onPostExecute(Long result) {
	    	 initMainActivity();
	     }
	}

	public void addPage(FragmentBase fragment) {
		FragmentManager fm;
		FragmentTransaction ft;

		fm = getSupportFragmentManager();
		ft = fm.beginTransaction();

		ft.replace(R.id.content_container, fragment);
		ft.addToBackStack(fragment.mStrPageID);
		ft.commit();
	}
	
	public void popLastPage() {
		FragmentManager fm = getSupportFragmentManager();
		fm.popBackStack();
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		
		Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
		if (mViewPager.getCurrentItem() == 0 && page != null) {

			// TODO Auto-generated method stub
			switch (item.getItemId()) {
			case R.id.filter_15:
				((FragmentHollering)page).updateListView(1);
				return true;
			case R.id.filter_30:
				((FragmentHollering)page).updateListView(2);
				return true;
			case R.id.filter_bored:
				((FragmentHollering)page).updateListView(3);
				return true;
			case R.id.filter_player:
				((FragmentHollering)page).updateListView(4);
				return true;
			case R.id.filter_hooked:
				((FragmentHollering)page).updateListView(5);
				return true;
			case R.id.filter_thirsty:
				((FragmentHollering)page).updateListView(6);
				return true;
			case R.id.filter_all:
				((FragmentHollering)page).updateListView(0);
				return true;
			default:
				return false;
			}
		} else {
			return false;
		}
	}
}