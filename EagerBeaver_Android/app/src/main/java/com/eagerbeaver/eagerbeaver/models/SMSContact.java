package com.eagerbeaver.eagerbeaver.models;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.eagerbeaver.eagerbeaver.models.DayStatsInfo.MsgMode;
import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo.MsgDirectionType;
import com.github.snowdream.android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SMSContact {
	public static final int TIME_SPAN = 3; //3 hours
	
	public String id; //id of contact 
	public ArrayList<String> addresses; //phone number
	public String name; //name will be retrieved later
	public String first_name; //first name which will be used in advice section
	
	public int cntMsgsBetween;
	public int cntMsgsSent;
	public int cntPictureMsgsBetween;
	public int cntPictureMsgsSent;
	
	public int totalLengthBetween;
	public int totalLengthBetweenSent;
	
	public long totalResponseGot;	// from them to me
	public long totalResponseTime; 
	
	public long totalRespondSent;	// from me to them
	public long totalRespondTime; 
	
	public ArrayList<DayStatsInfo> lastDaysStats;
	
	public String strLastType;
	public Date dateLastMsg;
	
	public int[] mBestTimes = new int [24 / TIME_SPAN];
	
	public SMSContact(String id, String name, String f_name) {
		this.id = id;
		this.name = name;
		if(f_name == null)
			this.first_name = name;
		else
			this.first_name = f_name;
		this.addresses = new ArrayList<String>();
		
		lastDaysStats = new ArrayList<DayStatsInfo>();
		for(int i = 0; i < TotalStatsInfo.NUM_DAYS_FOR_HISTORY + 1 ; i++) {
			lastDaysStats.add(new DayStatsInfo());
		}
		
		for(int i = 0; i < mBestTimes.length; i++) {
			mBestTimes[i] = 0;
		}
	}
	
	public SMSContact(String id, String address, String name, String f_name) {
		this.id = id;
		this.name = name;
		if(f_name == null)
			this.first_name = name;
		else
			this.first_name = f_name;
		this.addresses = new ArrayList<String>();
		this.addresses.add(refineNumber(address));
		
		lastDaysStats = new ArrayList<DayStatsInfo>();
		for(int i = 0; i < TotalStatsInfo.NUM_DAYS_FOR_HISTORY + 1 ; i++) {
			lastDaysStats.add(new DayStatsInfo());
		}
		
		for(int i = 0; i < mBestTimes.length; i++) {
			mBestTimes[i] = 0;
		}
	}
	
	public void processMessage(boolean incoming, boolean mms, Date date, int length, MsgDirectionType type, long time, long thread_id) {
		
		cntMsgsBetween++;

		if(mms) 
			cntPictureMsgsBetween++;
		
		if(!incoming) {
			cntMsgsSent++;
			if(mms) cntPictureMsgsSent++;
		}
		
		Log.i("processMessage: " + name + " " + date + " " + cntMsgsBetween + " " + cntMsgsSent + " " + cntPictureMsgsBetween + " " + cntPictureMsgsSent);

		if(time < 24 * 60 * 60) {
			if(type == MsgDirectionType.MsgDirectionIn) {
				totalResponseGot++;
				totalResponseTime += time;
			}
			else if(type == MsgDirectionType.MsgDirectionOut) {
				totalRespondSent++;
				totalRespondTime += time;
			}
		}
		
		totalLengthBetween += length;
		if(!incoming) {
			totalLengthBetweenSent += length;
		}
		
		Date nowDate = new Date();
		int daysFromToday = (int)((nowDate.getTime() - date.getTime()) / 1000 / 60 / 60 / 24);
		if(daysFromToday < TotalStatsInfo.NUM_DAYS_FOR_HISTORY) {
			// last x days trends
			lastDaysStats.get(daysFromToday).processMessage(incoming, mms, length, type, time);
		}
		else {
			lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY).processMessage(incoming, mms, length, type, time);
		}
		
		
		if(dateLastMsg != null) {
			if(dateLastMsg.getTime() < date.getTime()) {
				dateLastMsg = date;
				strLastType = mms ? "Picture"  : "Text";
			}
		} else {
			dateLastMsg = date;
			strLastType = mms ? "Picture"  : "Text";
		}
		
		
		@SuppressWarnings("deprecation")
		int hours = date.getHours();
		int hour_slot_idx = hours / TIME_SPAN;
		mBestTimes[hour_slot_idx]++;
		
		//////////////////////////////////// Thread processing //////////////////////////////////////
		if(time > 60 * 60 * 24) {
			//if it is one more day
			
		}
		if(_last_thread_id == thread_id) {
			//the same thread
			if(type == MsgDirectionType.MsgDirectionIn) {
				m_currentThreadResponseMin += time; 
			}
			else if(type == MsgDirectionType.MsgDirectionOut) {
				m_currentThreadRespondMin += time;
			}
		}
		else {
			//new thread
			if(m_currentThreadResponseMin == 0 && m_currentThreadRespondMin == 0) {
			
			}
			else {
				if(m_currentThreadResponseMin < m_currentThreadRespondMin) {
					m_threadsLead++;
				}
				else if(m_currentThreadRespondMin > m_currentThreadResponseMin) {
					m_threadsFollow++;
				}
				m_threadsCount++;
			}
			_last_thread_id = thread_id;
		}
	}
	
	public long m_threadsCount;
	public long m_threadsFollow;
	public long m_threadsLead;
	
	
	public long m_currentThreadResponseMin;
	public long m_currentThreadRespondMin;
	private long _last_thread_id = 0;

	/////////////////////////////////////////////////////////////
	
	public int getLastTextedPeriod() {
		Date nowDate = new Date();
		long passed_time;
		if (dateLastMsg != null) {
			passed_time = nowDate.getTime() - dateLastMsg.getTime();	
		} else {
			passed_time = 0;
		}
		
		return (int) ((long) passed_time / (long) (86400000));
	}
	
	public int getAveResponseTime()  {
		if(totalResponseGot == 0) {
			return 0;
		}
		return (int)(totalResponseTime / totalResponseGot);
	}
	
	public int getAveRespondTime()  {
		if(totalRespondSent == 0) {
			return 0;
		}
		return (int)(totalRespondTime / totalRespondSent);
	}
	
	
	public float getAveResTimesTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = (mode == MsgMode.MsgModeSend) ? info.m_totalResponds : info.m_totalResponsesGet;
		long msgRespTime = (mode == MsgMode.MsgModeSend) ? info.m_totalRespondsTime : info.m_totalResponsesGetTime;
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.m_totalResponds : info.m_totalResponsesGet;
			msgRespTime += (mode == MsgMode.MsgModeSend) ? info.m_totalRespondsTime : info.m_totalResponsesGetTime;
			
			values[i] = (msgCount == 0) ? 0 : msgRespTime / msgCount;
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	
	/////////////////////////////////////////////////////////////
	
	public long getAveCharLength(MsgMode mode) {
		if(mode == MsgMode.MsgModeSend) {
			return cntMsgsSent == 0 ? 0 : totalLengthBetweenSent / cntMsgsSent;
		}
		else if(mode == MsgMode.MsgModeRecv) {
			return (cntMsgsBetween - cntMsgsSent) == 0 ? 0 : (totalLengthBetween - totalLengthBetweenSent) / (cntMsgsBetween - cntMsgsSent);
		}
		return cntMsgsBetween == 0 ? 0 : totalLengthBetween / cntMsgsBetween;
	}
	
	public float getAveCharLengthTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
		long msgLength = (mode == MsgMode.MsgModeSend) ? info.totalLengthOut : info.totalLengthIn;
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
			msgLength += (mode == MsgMode.MsgModeSend) ? info.totalLengthOut : info.totalLengthIn;
			
			values[i] = (msgCount == 0) ? 0 : msgLength / msgCount;
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	/////////////////////////////////////////////////////////////
	
	
	public long getMessagesCountLast7Days(MsgMode mode) {
		int dayCount = Math.min(7, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = 0;
		for(int i = 0; i < dayCount; i++) {
			DayStatsInfo info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
		}
		return msgCount;
	}
	
	public float getMessageCountLast7DaysTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMsgs : info.numberOfIncomingMsgs;
			values[i] = msgCount;
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	/////////////////////////////////////////////////////////////
	
	public long getPictureMessagesCount(MsgMode mode) {
		if(mode == MsgMode.MsgModeRecv) {
			return cntPictureMsgsBetween - cntPictureMsgsSent;
		}
		else if(mode == MsgMode.MsgModeSend) {
			return cntPictureMsgsSent;
		}
		
		return cntPictureMsgsBetween;
	}
	
	public float getPictureMessageCountLast7DaysTrend(MsgMode mode) {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		long msgCount = (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMMS : info.numberOfIncomingMMS;
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			msgCount += (mode == MsgMode.MsgModeSend) ? info.numberOfOutgoingMMS : info.numberOfIncomingMMS;
			values[i] = msgCount;
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}
	
	public int getAveTimeBetween() {
		if(totalResponseGot + totalRespondSent == 0)
			return 0;
		return (int)((totalResponseTime + totalRespondTime) / (totalResponseGot + totalRespondSent));
	}
	
	
	public float getScoreToday() {
		DayStatsInfo info = lastDaysStats.get(0);
		return info.getScoreStat();
	}
	
	public float getAveLastScore() {
		float scoreSum = 0;
		for(int i = 0; i < TotalStatsInfo.NUM_DAYS_FOR_HISTORY; i++) {
			scoreSum += lastDaysStats.get(i).getScoreStat();
		}
		return scoreSum / TotalStatsInfo.NUM_DAYS_FOR_HISTORY;
	}
	
	public float getOverallScore() {
		return DayStatsInfo.getScore(cntMsgsBetween == 0 ? 0 : totalLengthBetween / cntMsgsBetween, (getAveResponseTime() + getAveRespondTime()) / 2, cntMsgsBetween);
	}
	
	public float getLastScoreTrend() {
		float values[] = new float[TotalStatsInfo.NUM_DAYS_FOR_HISTORY];
		
		DayStatsInfo info = lastDaysStats.get(TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		
		for(int i = TotalStatsInfo.NUM_DAYS_FOR_HISTORY - 1; i >= 0; i--) {
			info = lastDaysStats.get(i);
			values[i] = info.getScoreStat();
		}
		
		return DayStatsInfo.calculateTrend(values, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
	}

	public String getStrLastType() {
		return strLastType;
	}

	public void setStrLastType(String strLastType) {
		this.strLastType = strLastType;
	}

	public Date getDateLastMsg() {
		return dateLastMsg;
	}

	public void setDateLastMsg(Date dateLastMsg) {
		this.dateLastMsg = dateLastMsg;
	}
	
	public String getLastDateString() {
		if(dateLastMsg == null)
			return "";
		String format = "MMM dd, h:mm a ZZZZ";
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
		return sdf.format(dateLastMsg);
	}
	
	public int getBestTimeMsgCount() {
		int max = 0;
		for(int i = 0; i < mBestTimes.length; i++) {
			if(max < mBestTimes[i]) {
				max = mBestTimes[i];
			}
		}
		
		return max;
	}
	
	public long getLeadConversationCount() {
		//we need to take into account the last one because we checks and counts the conversations for lead and follow 
		//only when new thread id is got
		if(m_currentThreadResponseMin != 0 || m_currentThreadRespondMin != 0) {
			if(m_currentThreadResponseMin < m_currentThreadRespondMin) {
				return m_threadsLead + 1;
			}
		}
		return m_threadsLead;
	}
	
	public long getFollowConversationCount() {
		if(m_currentThreadResponseMin != 0 || m_currentThreadRespondMin != 0) {
			if(m_currentThreadResponseMin > m_currentThreadRespondMin) {
				return m_threadsFollow + 1;
			}
		}
		return m_threadsFollow;
	}
	
	private String refineNumber(String number) {
		return number.replace(" ", "").replace("+", "").replace("-", "").replace("(", "").replace(")", "");
	}

	public void addNumber(String number) {
		
		this.addresses.add(refineNumber(number));
	}
	
	public boolean hasNumber(String number) {
		if(this.addresses == null)
			return false;
		
		number = refineNumber(number);
		for(int i = 0; i < this.addresses.size(); i++) {
			String adr = this.addresses.get(i);
			if(adr.equals(number)) {
				return true;
			}
			
			if(adr.contains(number) || number.contains(adr))
				return true;
		}
		
		return false;
	}
	
	
	public int getRatioY() {
		float M = this.cntMsgsSent == 0 ? 5.0f : (float) (((float) this.cntMsgsBetween - (float) this.cntMsgsSent) / (float) this.cntMsgsSent);
		
		float newM = M;
		if(M <= 0.65f) {
			newM = M * 25 / 0.65f;
		}
		else if (M <= 0.8f){
			newM = 25.0f + (M - 0.65f) * (25.0f / 0.15f);
		}
		else if(M <= 1.20f) {
			newM = 50.0f + (M - 0.8f) * (25.0f / 0.40f);
		}
		else {
			newM = 75.0f + (M - 1.20f) * (25.0f / 3.8f);
		}
		
		float PM = this.cntPictureMsgsSent == 0 ? 5 : (this.cntPictureMsgsBetween - this.cntPictureMsgsSent) / this.cntPictureMsgsSent;
		
		float newPM = PM;
		if(PM <= 0.65f) {
			newPM = PM * 25.0f / 0.65f;
		}
		else if(PM <= 0.8f){
			newPM = 25.0f + (PM - 0.65f) * (25.0f / 0.15f);
		}
		else if(PM <= 1.20f) {
			newPM = 50.0f + (PM - 0.8f) * (25.0f / 0.40f);
		}
		else {
			newPM = 75.0f + (PM - 1.20f) * (25.0f / 3.8f);
		}
		
		float finalY = newM * 0.85f + newPM * 0.15f;
		
		if(finalY > 100.0f)
			finalY = 100.0f;
		else if(finalY < 0)
			finalY = 0.0f;
		
		return (int)finalY;
	}
	
	public int getRatioX() {
		float charLength = (float) this.getAveCharLength(MsgMode.MsgModeTotal);
		
		if(charLength <= 20.0f) {
			charLength = charLength * 25.0f / 20.0f;
		}
		else if(charLength <= 35.0f) {
			charLength = 25.0f + (charLength - 20.0f) * 25.0f / 10.0f;
		}
		else if(charLength <= 60.0f) {
			charLength = 50.0f + (charLength - 35.0f) * 25.0f / 25.0f;
		}
		else {
			charLength = 75.0f + (charLength - 60.0f) * 25.0f / 40.0f;
		}
		
		float resptime = (float) this.getAveResponseTime();
		if(resptime >= 90.0f) {
			resptime = 25.0f + (resptime - 90.0f) * -25.0f / 1350.0f;
		}
		else if(resptime >= 30.0f) {
			resptime = 50.0f + (resptime - 30.0f) * -25.0f / 60.0f;
		}
		else if(resptime >= 10.0f) {
			resptime = 75.0f + (resptime - 10.0f) * -25.0f / 20.0f;
		}
		else {
			resptime = 100.0f + (resptime) * -25.0f / 10.0f;
		}
		
		float finalX = (float) charLength * 0.3f + (float) resptime * 0.7f;
		
		if(finalX > 100.0f)
			finalX = 100.0f;
		else if(finalX < 0.0f)
			finalX = 0.0f;
		
		return (int)finalX;
	}
	
	public String getRegionNumber() {

		float cx = 100.0f;
		float cy = 100.0f;
		
		float x = -cx + 2.0f * cx * getRatioX() / 100.0f;
		float y = cy - 2.0f * cy * getRatioY() / 100.0f;
		
		if (x <= 0.0f && y >= 0.0f){
			if (x >= -(cx / 3.0f) && y <= cy / 3.0f) {
				return ("01");
			} else if (x >= -(2.0f * cx / 3.0f) && y <= (2.0f * cy / 3.0f)) {
				return ("02");
			} else {
				return ("03");
			}
		} else if (x <= 0.0f && y <= 0.0f) {
			if (x >= -(cx / 3.0f) && y >= -(cy / 3.0f)) {
				return ("11");
			} else if (x >= -(2.0f * cx / 3.0f) && y >= -(2.0f * cy / 3.0f)) {
				return ("12");
			} else {
				return ("13");
			}
		} else if (x >= 0.0f && y >= 0.0f) {
			if (x <= cx / 3.0f && y <= cy / 3.0f) {
				return ("21");
			} else if (x <= (2.0f * cx / 3.0f) && y <= (2.0f * cy / 3.0f)) {
				return ("22");
			} else {
				return ("23");
			}
		} else {
			if (x <= cx / 3.0f && y >= -(cy / 3.0f)) {
				return ("31");
			} else if (x <= (2.0f * cx / 3.0f) && y >= -(2.0f * cy / 3.0f)) {
				return ("32");
			} else {
				return ("33");
			}
		}
	}
	
	
	private Uri mUri = null;
	/**
	 * @return the photo URI
	 */
	public Uri getPhotoUri(Context context) {
		
		if(mUri != null)
			return mUri;
		
	    try {
	        Cursor cur = context.getContentResolver().query(
	                ContactsContract.Data.CONTENT_URI,
	                null,
	                ContactsContract.Data.CONTACT_ID + "=" + this.id + " AND "
	                        + ContactsContract.Data.MIMETYPE + "='"
	                        + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null,
	                null);
	        try {
	        	if (cur != null) {
		            if (!cur.moveToFirst()) {
		                return null; // no photo
		            }
		        } else {
		            return null; // error in cursor process
		        }	
	        } finally {
	        	cur.close();
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	    
	    Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(this.id));
	    
	    InputStream is = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(), person);

	    if (is == null) {
	     // Your contact doesn't have a valid photo 
	     // i.e. use the default photo for your app
	    	return null;
	    } else {
	    	try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	// This will always succeed when assigned to an ImageView!
	    	mUri = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
	    	return mUri; 
	    }
	}
		
}
