package com.eagerbeaver.eagerbeaver.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.eagerbeaver.eagerbeaver.BaseApplication;
import com.eagerbeaver.eagerbeaver.MainActivity;
import com.eagerbeaver.eagerbeaver.R;
import com.eagerbeaver.eagerbeaver.models.DayStatsInfo.MsgMode;
import com.eagerbeaver.eagerbeaver.models.SMSContact;
import com.eagerbeaver.eagerbeaver.models.TotalStatsInfo;
import com.eagerbeaver.eagerbeaver.utils.Utils;
import com.eagerbeaver.eagerbeaver.widgets.AvatarImageView;
import com.eagerbeaver.eagerbeaver.widgets.MyMarkerView;
import com.eagerbeaver.eagerbeaver.widgets.PhotoCircleView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Legend;
import com.github.mikephil.charting.utils.Legend.LegendForm;
import com.github.mikephil.charting.utils.Legend.LegendPosition;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.XLabels.XLabelPosition;
import com.github.snowdream.android.util.Log;
import com.han.utility.TimeUtility;
import com.jjoe64.graphview.LineGraphView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class FragmentHolleringDetail extends FragmentBase implements
		OnClickListener, OnChartValueSelectedListener {

	public static String PAGEID = "HOLLERINGDETAIL";

	// constructor
	public FragmentHolleringDetail() {
		super();
		mStrPageID = PAGEID;
	}

	TotalStatsInfo mInfo;

	RelativeLayout btn_peeps_profile;
	RelativeLayout btn_peeps_resptime;
	RelativeLayout btn_peeps_msgcount;

	ImageView img_peeps_profile;
	ImageView img_peeps_resptime;
	ImageView img_peeps_msgcount;

	View btn_profileindicator;
	View btn_peeps_respindicator;
	View btn_peeps_msgindicator;

	ScrollView peeps_page_profile;
	LinearLayout peeps_page_resptime;
	LinearLayout peeps_page_msgcount;

	RelativeLayout peeps_page_back;

	// Profile page
	ImageView btn_share;
	PhotoCircleView img_contact;
	AvatarImageView btn_profile_interest;
	ImageView interest_bubble;
	Boolean interest_bubble_on = false;

	TextView txt_profile_msg_sent;
	TextView txt_profile_msg_received;

	TextView txt_actual_time;

	TextView mTvPslLabel;
	TextView mTvPslContent;

	ImageView time_section_1;
	ImageView time_section_2;
	ImageView time_section_3;
	ImageView time_section_4;

	// Response Page

	TextView txt01;
	TextView txt02;
	TextView txt11;
	TextView txt12;
	TextView txt01_ave;
	TextView txt02_ave;
	TextView txt11_ave;
	TextView txt12_ave;

	LineChart resp_chart;

	// Message Page
	TextView txt001;
	TextView txt002;
	TextView txt011;
	TextView txt012;
	TextView txt001_ave;
	TextView txt002_ave;
	TextView txt011_ave;
	TextView txt012_ave;

	BarChart msg_chart;

	// ArrayList<SMSContact> mContactList;
	SMSContact mContact;

	TextView mTvName;
	TextView mTvRespons;
	TextView mTvScore;
	TextView mTvStatusLabel;
	TextView mTvStatus;

	GridView mGvStats;
	GridView mGvBestTime;

	FrameLayout mGraphContainer;
	LineGraphView mGraphView;
	
	String str_personal;

	public void setValue(SMSContact contact) {
		mContact = contact;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mMainActivity = (MainActivity) getActivity();
		FrameLayout view = (FrameLayout) inflater.inflate(
				R.layout.fragment_hollering_detail, container, false);

		mRootLayout = view;

		mInfo = ((BaseApplication) getActivity().getApplication())
				.getTotalStatsInfo();

		btn_peeps_profile = (RelativeLayout) view
				.findViewById(R.id.btn_peeps_profile);
		btn_peeps_resptime = (RelativeLayout) view
				.findViewById(R.id.btn_peeps_resptime);
		btn_peeps_msgcount = (RelativeLayout) view
				.findViewById(R.id.btn_peeps_msgcount);

		img_peeps_profile = (ImageView) view
				.findViewById(R.id.img_peeps_profile);
		img_peeps_resptime = (ImageView) view
				.findViewById(R.id.img_peeps_resptime);
		img_peeps_msgcount = (ImageView) view
				.findViewById(R.id.img_peeps_msgcount);

		btn_profileindicator = (View) view
				.findViewById(R.id.btn_profileindicator);
		btn_peeps_respindicator = (View) view
				.findViewById(R.id.btn_peeps_respindicator);
		btn_peeps_msgindicator = (View) view
				.findViewById(R.id.btn_peeps_msgindicator);

		peeps_page_profile = (ScrollView) view
				.findViewById(R.id.peeps_page_profile);
		peeps_page_resptime = (LinearLayout) view
				.findViewById(R.id.peeps_page_resptime);
		peeps_page_msgcount = (LinearLayout) view
				.findViewById(R.id.peeps_page_msgcount);

		btn_peeps_profile.setOnClickListener(this);
		btn_peeps_resptime.setOnClickListener(this);
		btn_peeps_msgcount.setOnClickListener(this);

		peeps_page_back = (RelativeLayout) view
				.findViewById(R.id.peeps_page_back);
		peeps_page_back.setOnClickListener(this);

		// Peeps Profile page
		btn_share = (ImageView) view.findViewById(R.id.btn_share);
		btn_share.setOnClickListener(this);
		
		mTvName = (TextView) view.findViewById(R.id.txt_name);

		img_contact = (PhotoCircleView) view.findViewById(R.id.img_contact);
		btn_profile_interest = (AvatarImageView) view
				.findViewById(R.id.btn_profile_interest);

		txt_profile_msg_sent = (TextView) view
				.findViewById(R.id.txt_profile_msg_sent);
		txt_profile_msg_received = (TextView) view
				.findViewById(R.id.txt_profile_msg_received);

		time_section_1 = (ImageView) view.findViewById(R.id.img_time_1);
		time_section_2 = (ImageView) view.findViewById(R.id.img_time_2);
		time_section_3 = (ImageView) view.findViewById(R.id.img_time_3);
		time_section_4 = (ImageView) view.findViewById(R.id.img_time_4);

		txt_actual_time = (TextView) view.findViewById(R.id.txt_actual_time);
		// mTvPslLabel =
		// (TextView)view.findViewById(R.id.txt_personality_label);
		mTvPslContent = (TextView) view
				.findViewById(R.id.txt_personality_content);

		// Response Page

		txt01 = (TextView) view.findViewById(R.id.txt01);
		txt02 = (TextView) view.findViewById(R.id.txt02);
		txt11 = (TextView) view.findViewById(R.id.txt11);
		txt12 = (TextView) view.findViewById(R.id.txt12);

		// Set Rounded Text Color
		txt01_ave = (TextView) view.findViewById(R.id.txt01_ave);
		txt02_ave = (TextView) view.findViewById(R.id.txt02_ave);
		txt11_ave = (TextView) view.findViewById(R.id.txt11_ave);
		txt12_ave = (TextView) view.findViewById(R.id.txt12_ave);

		resp_chart = (LineChart) view.findViewById(R.id.resp_chart);

		// Message Page
		txt001 = (TextView) view.findViewById(R.id.txt001);
		txt002 = (TextView) view.findViewById(R.id.txt002);
		txt011 = (TextView) view.findViewById(R.id.txt011);
		txt012 = (TextView) view.findViewById(R.id.txt012);

		// Set Rounded Text Color
		txt001_ave = (TextView) view.findViewById(R.id.txt001_ave);
		txt002_ave = (TextView) view.findViewById(R.id.txt002_ave);
		txt011_ave = (TextView) view.findViewById(R.id.txt011_ave);
		txt012_ave = (TextView) view.findViewById(R.id.txt012_ave);
		
		// Future Update
		txt01_ave.setVisibility(View.GONE);
		txt02_ave.setVisibility(View.GONE);
		txt11_ave.setVisibility(View.GONE);
		txt12_ave.setVisibility(View.GONE);
		txt001_ave.setVisibility(View.GONE);
		txt002_ave.setVisibility(View.GONE);
		txt011_ave.setVisibility(View.GONE);
		txt012_ave.setVisibility(View.GONE);
		

		msg_chart = (BarChart) view.findViewById(R.id.msg_chart);

		mGraphContainer = (FrameLayout) view.findViewById(R.id.graph_container);

		setValues();
		return view;
	}

	public void setValues() {

		mTvName.setText(mContact.name);

		/* Profile Page */

		Uri uri = mContact.getPhotoUri(this.getActivity());
		if (uri != null) {
			img_contact.setImageData(uri);
		} else {
			img_contact.setImageResource(R.drawable.ic_launcher);
		}

		// Set avatar image and lbl interest
		int image_id = 0;

		if (mContact.getRegionNumber().charAt(0) == '0') {
			image_id = 0;
			mTvPslContent.setBackgroundColor(getResources().getColor(R.color.color_bored_bg));
		} else if (mContact.getRegionNumber().charAt(0) == '1') {
			image_id = 1;
			mTvPslContent.setBackgroundColor(getResources().getColor(R.color.color_player_bg));
		} else if (mContact.getRegionNumber().charAt(0) == '2') {
			image_id = 2;
			mTvPslContent.setBackgroundColor(getResources().getColor(R.color.color_hooked_bg));
		} else {
			image_id = 3;
			mTvPslContent.setBackgroundColor(getResources().getColor(R.color.color_thirsty_bg));
		}
		str_personal = Utils.getSuggestionMessage(mContact.first_name, mContact.getRegionNumber());
		
		
		mTvPslContent.setText(str_personal);

		btn_profile_interest.setAvatarId(image_id);

		txt_profile_msg_sent.setText(String.valueOf(mContact.cntMsgsSent));
		txt_profile_msg_received.setText(String.valueOf(mContact.cntMsgsBetween
				- mContact.cntMsgsSent));

		time_section_1.setVisibility(View.GONE);
		time_section_2.setVisibility(View.GONE);
		time_section_3.setVisibility(View.GONE);
		time_section_4.setVisibility(View.GONE);

		int max = mContact.getBestTimeMsgCount();

		String str_best_time = "";
		int time_section = 0;
		for (int i = 0; i < mContact.mBestTimes.length; i++) {
			if (max == mContact.mBestTimes[i]) {
				if (str_best_time.length() != 0) {
					str_best_time += ", ";
				}
				time_section = i * SMSContact.TIME_SPAN;
				str_best_time += time_section + " ~ "
						+ (time_section + SMSContact.TIME_SPAN);

				if (time_section == 0 || time_section == 12) {
					time_section_1.setVisibility(View.VISIBLE);
				} else if (time_section == 3 || time_section == 15) {
					time_section_2.setVisibility(View.VISIBLE);
				} else if (time_section == 6 || time_section == 18) {
					time_section_3.setVisibility(View.VISIBLE);
				} else if (time_section == 9 || time_section == 21) {
					time_section_4.setVisibility(View.VISIBLE);
				}
			}
		}
		txt_actual_time.setText(str_best_time);
		// mTvPslLabel.setText("Personality label: Thirsty");

		/* RespTime Page */
//		String.format("%.2f", (float)(mInfo.getAveRespondTime() / 60))
		float trends = 0;
		txt01.setText(String.format("%.2f", (float)(mContact.getAveResponseTime() / 60)));
		trends = mContact.getAveResTimesTrend(MsgMode.MsgModeRecv);
		setAveBgText(trends, txt01_ave);
		
		txt02.setText(String.format("%.2f", (float)(mContact.getAveRespondTime() / 60)));
		trends = mContact.getAveResTimesTrend(MsgMode.MsgModeSend);
		setAveBgText(trends, txt02_ave);
		
		txt11.setText(String.valueOf(mContact.getAveCharLength(MsgMode.MsgModeSend)));
		trends = mContact.getAveCharLengthTrend(MsgMode.MsgModeSend);
		setAveBgText(trends, txt11_ave);
		
		txt12.setText(String.valueOf(mContact.getAveCharLength(MsgMode.MsgModeRecv)));
		trends = mContact.getAveCharLengthTrend(MsgMode.MsgModeRecv);
		setAveBgText(trends, txt12_ave);
		
		/* Message Page */

		txt001.setText(String.valueOf(mContact.getMessagesCountLast7Days(MsgMode.MsgModeRecv)));
		trends = mContact.getMessagesCountLast7Days(MsgMode.MsgModeRecv);
		setAveBgText(trends, txt001_ave);
		
		txt002.setText(String.valueOf(mContact.getMessagesCountLast7Days(MsgMode.MsgModeSend)));
		trends = mContact.getMessagesCountLast7Days(MsgMode.MsgModeSend);
		setAveBgText(trends, txt002_ave);
		
		txt011.setText(String.valueOf(mContact.getPictureMessagesCount(MsgMode.MsgModeSend)));
		trends = mContact.getPictureMessageCountLast7DaysTrend(MsgMode.MsgModeSend);
		setAveBgText(trends, txt011_ave);
		
		txt012.setText(String.valueOf(mContact.getPictureMessagesCount(MsgMode.MsgModeRecv)));
		trends = mContact.getPictureMessageCountLast7DaysTrend(MsgMode.MsgModeRecv);
		setAveBgText(trends, txt012_ave);
	}

	private void setAveBgText(Float v_trends, TextView t_view) {
		GradientDrawable drawable = (GradientDrawable) t_view.getBackground();
		if (v_trends > 0) {
			drawable.setColor(getResources().getColor(R.color.color_green));
			t_view.setText("+" + Float.toString(v_trends) + "%");	
		} else if (v_trends == 0){
			drawable.setColor(getResources().getColor(R.color.color_red));
			t_view.setText("+" + Float.toString(v_trends) + "%");
		} else {
			drawable.setColor(getResources().getColor(R.color.color_red));
			t_view.setText(Float.toString(v_trends) + "%");
		}
	}
	
	public class InfoPair {
		String strName;
		String strValue;
		boolean bMain;

		public InfoPair(String name, String value) {
			strName = name;
			strValue = value;
		}
	}

	public class StatsAdapter extends BaseAdapter {
		ArrayList<InfoPair> mPairs;
		// Gets the context so it can be used later
		public StatsAdapter(Context c, ArrayList<InfoPair> pair) {
			mPairs = pair;
		}

		// Total number of things contained within the adapter
		public int getCount() {
			return mPairs.size();
		}

		// Require for structure, not really used in my code.
		public Object getItem(int position) {
			return mPairs.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) mMainActivity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.hollering_gen_stats_item, parent, false);
			}

			TextView txtName = (TextView) convertView
					.findViewById(R.id.txt_label);
			TextView txtValue = (TextView) convertView
					.findViewById(R.id.txt_value);

			InfoPair pair = (InfoPair) getItem(position);

			if (pair != null) {
				txtName.setText(pair.strName);
				txtValue.setText(pair.strValue);

				if (pair.strName == null || pair.strName.length() == 0) {
					txtValue.setGravity(Gravity.CENTER_VERTICAL
							| Gravity.CENTER_HORIZONTAL);
				}

				if (pair.bMain) {
					txtValue.setBackgroundColor(Color.GRAY);
				}
			}

			return convertView;
		}
	}
	
	Bitmap myBitmap;
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_peeps_profile:
			tabButtonClickedAction(0);
			break;
		case R.id.btn_peeps_resptime:
			tabButtonClickedAction(1);
			break;
		case R.id.btn_peeps_msgcount:
			tabButtonClickedAction(2);
			break;
		case R.id.peeps_page_back:
			mMainActivity.popLastPage();
			break;
		case R.id.btn_share:
			View v1 = peeps_page_profile;
			v1.setDrawingCacheEnabled(true);
			myBitmap = v1.getDrawingCache();
			saveBitmap(myBitmap);
//			showShareActionSheet();
			break;
		default:
			break;
		}
	}

	public void tabButtonClickedAction(int position) {
		switch (position) {
		case 0:
			btn_profileindicator.setBackgroundColor(this.getResources()
					.getColor(R.color.tab_bg));
			btn_peeps_respindicator.setBackgroundColor(Color.TRANSPARENT);
			btn_peeps_msgindicator.setBackgroundColor(Color.TRANSPARENT);

			img_peeps_profile.setImageResource(R.drawable.ico_peepsprofile_on);
			img_peeps_resptime.setImageResource(R.drawable.ico_resptime_off);
			img_peeps_msgcount.setImageResource(R.drawable.ico_msgcount_off);

			peeps_page_profile.setVisibility(View.VISIBLE);
			peeps_page_resptime.setVisibility(View.GONE);
			peeps_page_msgcount.setVisibility(View.GONE);
			break;
		case 1:
			btn_profileindicator.setBackgroundColor(Color.TRANSPARENT);
			btn_peeps_respindicator.setBackgroundColor(this.getResources()
					.getColor(R.color.tab_bg));
			btn_peeps_msgindicator.setBackgroundColor(Color.TRANSPARENT);

			img_peeps_profile.setImageResource(R.drawable.ico_peepsprofile_off);
			img_peeps_resptime.setImageResource(R.drawable.ico_resptime_on);
			img_peeps_msgcount.setImageResource(R.drawable.ico_msgcount_off);

			peeps_page_profile.setVisibility(View.GONE);
			peeps_page_resptime.setVisibility(View.VISIBLE);
			peeps_page_msgcount.setVisibility(View.GONE);

			drawLineChart();
			break;
		case 2:
			btn_profileindicator.setBackgroundColor(Color.TRANSPARENT);
			btn_peeps_respindicator.setBackgroundColor(Color.TRANSPARENT);
			btn_peeps_msgindicator.setBackgroundColor(this.getResources()
					.getColor(R.color.tab_bg));

			img_peeps_profile.setImageResource(R.drawable.ico_peepsprofile_off);
			img_peeps_resptime.setImageResource(R.drawable.ico_resptime_off);
			img_peeps_msgcount.setImageResource(R.drawable.ico_msgcount_on);

			peeps_page_profile.setVisibility(View.GONE);
			peeps_page_resptime.setVisibility(View.GONE);
			peeps_page_msgcount.setVisibility(View.VISIBLE);

			drawBarChart();
			break;
		default:
			break;
		}
	}
	
	private void showShareActionSheet(String mPath) {
		Log.v("holleringdetail showShareActionSheet path", mPath);
		Uri capture_img_uri = Uri.fromFile(new File(mPath));
		Intent shareIntent = new Intent();
		shareIntent.setAction(Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, "EagerBeaver");
		shareIntent.putExtra(Intent.EXTRA_STREAM, capture_img_uri);
		shareIntent.setType("image/jpeg");
		shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		
		startActivity(Intent.createChooser(shareIntent, "Share via"));
	}
	
	private void saveBitmap(Bitmap bitmap) {
		
		Date date = new Date();
		String filePath = Environment.getExternalStorageDirectory()
				+ File.separator + "Pictures/screenshot" + DateFormat.format("yyMMddhhmmss", date) + ".png";
		File imagePath = new File(filePath);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(imagePath);
			bitmap.compress(CompressFormat.PNG, 100, fos);
			fos.flush();
			fos.close();
			showShareActionSheet(filePath);
		} catch (FileNotFoundException e) {
            Log.e("HolleringDetail saveBitmap Exception", e);
		} catch (IOException e) {
            Log.e("HolleringDetail saveBitmap Exception", e);
		}
	}

	private void drawLineChart() {
		resp_chart.setOnChartValueSelectedListener(this);
		resp_chart.setStartAtZero(true);

		resp_chart.setDrawYValues(false);
		resp_chart.setDrawYLabels(false);

		resp_chart.setGridColor(Color.TRANSPARENT);
		resp_chart.setDrawGridBackground(false);

		resp_chart.setDrawBorder(false);

		resp_chart
				.setNoDataTextDescription("You need to provide data for the chart.");
		resp_chart.setDescription("");

		resp_chart.setHighlightEnabled(true);
		resp_chart.setTouchEnabled(true);

		resp_chart.setPinchZoom(true);

		MyMarkerView mv_bubble = new MyMarkerView(mMainActivity,
				R.layout.custom_marker_view);
		resp_chart.setMarkerView(mv_bubble);

		resp_chart.setHighlightIndicatorEnabled(false);

		setLineData(7, 10);

		Legend l = resp_chart.getLegend();
		l.setForm(LegendForm.CIRCLE);
		l.setPosition(LegendPosition.RIGHT_OF_CHART_INSIDE);
		l.setTextColor(getResources().getColor(R.color.color_text_main));
		l.setTextSize(10.0f);

		XLabels xl = resp_chart.getXLabels();

		xl.setSpaceBetweenLabels(0);
		xl.setAdjustXLabels(true);
		xl.setPosition(XLabelPosition.BOTTOM);
	}

	private void setLineData(int count, float range) {

		ArrayList<String> xVals = new ArrayList<String>();
		Date nowDate = new Date();
		for (int i = count; i > 0; i--) {
//			xVals.add("Day " + (i + 1));
			Date date = new Date();
			date.setTime(nowDate.getTime() - (long) 86400000 * (long) (i - 1));
			xVals.add(TimeUtility.getStringFromDate(date, "MM/dd"));
		}

		ArrayList<Entry> yVals_you = new ArrayList<Entry>();
		ArrayList<Entry> yVals_contact = new ArrayList<Entry>();

		
		int weekDaysCount = Math.min(count, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		for (int i = 0; i < weekDaysCount; i++) {
			float val = mContact.lastDaysStats.get(weekDaysCount - i - 1).getAveRespondTime();
			yVals_you.add(new BarEntry(val, i));
		}

		for (int i = 0; i < weekDaysCount; i++) {
			float val = mContact.lastDaysStats.get(weekDaysCount - i - 1).getAveResponseTime();
			yVals_contact.add(new BarEntry(val, i));
		}

		// create a dataset and give it a type
		LineDataSet dataset_you = new LineDataSet(yVals_you, "You");

		dataset_you.setColor(getResources().getColor(R.color.chart_you));
		dataset_you.setCircleColor(Color.TRANSPARENT);
		dataset_you.setLineWidth(3f);
		dataset_you.setCircleSize(1f);

		// create a dataset and give it a type
		LineDataSet dataset_contact = new LineDataSet(yVals_contact,
				"Your peeps");

		dataset_contact
				.setColor(getResources().getColor(R.color.chart_contact));
		dataset_contact.setCircleColor(Color.TRANSPARENT);
		dataset_contact.setFillColor(Color.TRANSPARENT);
		dataset_contact.setLineWidth(3f);
		dataset_contact.setCircleSize(1f);

		ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
		dataSets.add(dataset_you); // add the datasets
		dataSets.add(dataset_contact); // add the datasets

		// create a data object with the datasets
		LineData data = new LineData(xVals, dataSets);

		// set data
		resp_chart.setData(data);
		resp_chart.animateX(3000);
		resp_chart.invalidate();
	}

	private void drawBarChart() {
		msg_chart.setOnChartValueSelectedListener(this);
		msg_chart.setStartAtZero(true);

		msg_chart.setDrawYValues(false);
		msg_chart.setDrawYLabels(false);

		msg_chart.setGridColor(Color.TRANSPARENT);
		msg_chart.setDrawGridBackground(false);
		msg_chart.setDrawBarShadow(false);

		msg_chart.setDrawBorder(false);

		msg_chart
				.setNoDataTextDescription("You need to provide data for the chart.");
		msg_chart.setDescription("");

		msg_chart.setHighlightEnabled(true);
		msg_chart.setTouchEnabled(true);

		msg_chart.setPinchZoom(true);

		MyMarkerView mv_bubble = new MyMarkerView(mMainActivity,
				R.layout.custom_marker_view);
		msg_chart.setMarkerView(mv_bubble);

		msg_chart.setHighlightIndicatorEnabled(false);

		setBarData(7, 10);

		Legend l = msg_chart.getLegend();
		l.setForm(LegendForm.CIRCLE);
		l.setPosition(LegendPosition.RIGHT_OF_CHART_INSIDE);
		l.setTextColor(getResources().getColor(R.color.color_text_main));
		l.setTextSize(10.0f);

		XLabels xl = msg_chart.getXLabels();

		xl.setSpaceBetweenLabels(2);
		xl.setAdjustXLabels(true);
		xl.setPosition(XLabelPosition.BOTTOM);
	}

	private void setBarData(int count, float range) {

		ArrayList<String> xVals = new ArrayList<String>();
		Date nowDate = new Date();
		for (int i = count; i > 0; i--) {
//			xVals.add("Day " + (i + 1));
			Date date = new Date();
			date.setTime(nowDate.getTime() - (long) 86400000 * (long) (i - 1));
			xVals.add(TimeUtility.getStringFromDate(date, "MM/dd"));
		}

		ArrayList<BarEntry> yVals_you = new ArrayList<BarEntry>();
		ArrayList<BarEntry> yVals_contact = new ArrayList<BarEntry>();

		int weekDaysCount = Math.min(count, TotalStatsInfo.NUM_DAYS_FOR_HISTORY);
		for (int i = 0; i < weekDaysCount; i++) {
			float val = mContact.lastDaysStats.get(weekDaysCount - i - 1).getNumberOfMessages(MsgMode.MsgModeSend);
			yVals_you.add(new BarEntry(val, i));
		}

		for (int i = 0; i < weekDaysCount; i++) {
			float val = mContact.lastDaysStats.get(weekDaysCount - i - 1).getNumberOfMessages(MsgMode.MsgModeRecv);
			yVals_contact.add(new BarEntry(val, i));
		}
		// for(int i = 0; i < TotalStatsInfo.NUM_DAYS_FOR_HISTORY; i++) {
		// DayStatsInfo info = mInfo.m_LastDaysInfo.get(i);
		// yVals_you.add(new BarEntry(info.getAveTime() / 60, i));
		// yVals_contact.add(new BarEntry(info.getAveResponseTime() / 60, i));
		// }

		// create a dataset and give it a type
		BarDataSet dataset_you = new BarDataSet(yVals_you, "You");
		dataset_you.setColor(getResources().getColor(R.color.chart_you));

		BarDataSet dataset_contact = new BarDataSet(yVals_contact, "Your peeps");
		dataset_contact
				.setColor(getResources().getColor(R.color.chart_contact));

		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(dataset_you); // add the datasets
		dataSets.add(dataset_contact); // add the datasets

		// create a data object with the datasets
		BarData data = new BarData(xVals, dataSets);

		data.setGroupSpace(110f);

		// set data
		msg_chart.setData(data);
		msg_chart.animateY(3000);
		msg_chart.invalidate();
	}

	@Override
	public void onValueSelected(Entry e, int dataSetIndex) {
		// TODO Auto-generated method stub
		Log.i("VAL Selected",
				"Value: " + e.getVal() + ", xIndex: " + e.getXIndex()
						+ ", DateSet index: " + dataSetIndex);
	}

	@Override
	public void onNothingSelected() {
		// TODO Auto-generated method stub

	}
}
